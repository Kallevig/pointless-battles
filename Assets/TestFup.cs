﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestFup : MonoBehaviour {

    float MySpd;
    public int FrameWalk;
    int HorKey, VerKey;
    int WalkToDir;
    public bool Remote;
    public KeyCode KeyUp, KeyDown, KeyLeft, KeyRight;
    public GameObject Following;
    Locomotion Loc,FollowLoc;
    NavMap Navigator;
	// Use this for initialization
	void Start () {
        Navigator = GameObject.Find("_manager").GetComponent<Management>().Navigator;
        MySpd = 1f / FrameWalk;
        HorKey = 0;
        VerKey = 0;
        Loc = GetComponent<Locomotion>();
        WalkToDir = 0;
	}
    public float ReadMySpd()
    {
        return MySpd;
    }
    public int Coz(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Cos(_dir * Mathf.PI / 4)));
    }
    public int Zin(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Sin(_dir * Mathf.PI / 4)));
    }
    int PointDir(int x0, int y0, int x1, int y1)
    {
        int TempDir = 0;
        float dx = x1 - x0;
        float dy = y1 - y0;
        float rr = Mathf.Sqrt(dx * dx + dy * dy);
        if (dy > 0)
        {
            TempDir = Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else if (dy < 0)
        {
            TempDir = 8 - Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else if (rr > 0)
        {
            TempDir = Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else
        {
            Debug.LogError("You attempted to check the difference between the same coords");
        }

        return TempDir;

    }

    // Update is called once per frame
    void Update () {
        HorKey = 0;
        VerKey = 0;

        if (Remote)
        {
            if (Input.GetKey(KeyLeft)) { HorKey -= 1; }
            if (Input.GetKey(KeyRight)) { HorKey += 1; }
            if (Input.GetKey(KeyUp)) { VerKey += 1; }
            if (Input.GetKey(KeyDown)) { VerKey -= 1; }
        }else{
            if(!Loc.Moving){
                if(Following!=null){
                    FollowLoc = Following.GetComponent<Locomotion>();
                    float ddx = Loc.X - FollowLoc.X;
                    float ddy = Loc.Y - FollowLoc.Y;
                    if(Mathf.Sqrt(ddx*ddx+ddy*ddy) >1.5f ){
                        Coord pFl = Navigator.FindPathEx(Loc.X, Loc.Y, FollowLoc.X, FollowLoc.Y)[1];
                        Loc.TakeStep(PointDir(Loc.X, Loc.Y, pFl.x, pFl.y), MySpd);
                    }
                }
            }
        }

        if (HorKey != 0 || VerKey != 0)
        {
            WalkToDir = PointDir(0, 0, HorKey, VerKey);
            Loc.TakeStep(WalkToDir, MySpd);
        }


    }
}
