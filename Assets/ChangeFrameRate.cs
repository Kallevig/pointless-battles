﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeFrameRate : MonoBehaviour {
    public int FrameRate;
	// Use this for initialization
	void Start () {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = FrameRate;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
