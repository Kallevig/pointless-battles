﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMovement : MonoBehaviour {

    private Pathfinder ptf;
    WorldMap mp;
    public NavMap nvm;
    Coord MyPos,Goal;
    int Diry;
    Transform GT;
    List<Coord> MyPath;
	// Use this for initialization
	void Start () {
        mp = GameObject.Find("_manager").GetComponent<Management>().OverWorld;
        nvm = GameObject.Find("_manager").GetComponent<Management>().Navigator;
        ptf = new Pathfinder(mp);
        MyPos = new Coord(Mathf.RoundToInt(transform.position.x),
                          Mathf.RoundToInt(transform.position.z));
        GT = GameObject.FindWithTag("Goal").GetComponent<Transform>();
        Goal = new Coord(Mathf.RoundToInt(GT.position.x),
                         Mathf.RoundToInt(GT.position.z));
        Diry = 0;
        MyPath = new List<Coord>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.A))
        {
            MyPos.x = Mathf.RoundToInt(transform.position.x);
            MyPos.y = Mathf.RoundToInt(transform.position.z);
            Diry = ptf.FindDirection(MyPos, Goal, 0);
            transform.position = transform.position + new Vector3(ptf.Coz(Diry),0,ptf.Zin(Diry));
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Coord adf;
            MyPos.x = Mathf.RoundToInt(transform.position.x);
            MyPos.y = Mathf.RoundToInt(transform.position.z);
            adf = ptf.FindCoord(MyPos, Goal, 0);
            transform.position = new Vector3(adf.x, 0, adf.y);

        }
        MyPos.x = Mathf.RoundToInt(transform.position.x);
        MyPos.y = Mathf.RoundToInt(transform.position.z);
        MyPath = nvm.FindPath(MyPos.x,MyPos.y, Goal.x,Goal.y);
        for (int i = 0; i < MyPath.Count - 1; i += 1)
        {
            //Debug.Log("Drawing...");
            Debug.DrawLine(new Vector3(MyPath[i].x+0.5f, 0, MyPath[i].y+0.5f),
                            new Vector3(MyPath[i + 1].x+0.5f, 0, MyPath[i + 1].y+0.5f),
                           Color.white, 0f);
        }
    }
}
