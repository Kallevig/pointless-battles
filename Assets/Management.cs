﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Management : MonoBehaviour {

    public WorldMap OverWorld;
    public NavMap Navigator;
    public int WorldSize;
    public float WorldTime;
    public GameObject View;
    public GameResources GR;
    public Camera _main;
    // Use this for initialization
    private void Awake()
    {
        OverWorld = new WorldMap();
        Navigator = new NavMap(WorldSize, WorldSize);
    }
    void Start()
    {
        GR = GetComponent<GameResources>();
    }
    public void PlaceBlock(Vector3 BlockPos)
    {
        int xx = Mathf.RoundToInt(BlockPos.x);
        int yy = Mathf.RoundToInt(BlockPos.z);
        Navigator.BlockMap[xx,yy] = 1;
    }
    public void PlaceBlock(int x0, int y0)
    {
        Navigator.BlockMap[x0, y0] = 1;
    }
    public void PlaceObstacle(Vector3 BlockPos)
    {
        int xx = Mathf.RoundToInt(BlockPos.x);
        int yy = Mathf.RoundToInt(BlockPos.z);
        Navigator.BlockMap[xx, yy] = 2;
    }
    public void PlaceObstacle(int x0, int y0)
    {
        Navigator.BlockMap[x0, y0] = 2;
    }
    public void FreeSpace(Vector3 p0)
    {
        int xx = Mathf.RoundToInt(p0.x);
        int yy = Mathf.RoundToInt(p0.z);
        Navigator.BlockMap[xx, yy] = 0;
    }
    public void FreeSpace(int x0, int y0)
    {
        Navigator.BlockMap[x0, y0] = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0))
        {
            Click();
        }
        if (Input.GetMouseButtonDown(2))
        {
            View = null;
        }
	}

    void Click()
    {
        Ray _ray = _main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(_ray, out hit))
        {
            GameObject gmob = hit.collider.gameObject;
            if (gmob.tag == "BattlePiece") { View = gmob; }
        }
    }

    public void Generate(PieceInfo PInfo){
        if (!PInfo.generated){
            BaseInfo tmp = GR.BaseInfos[PInfo.species];
            //*
            PInfo.name = tmp.name;
            PInfo.description = tmp.description;
            PInfo.HP = tmp.hp;
            PInfo.hp = tmp.hp;
            PInfo.Atk = tmp.atk;
            PInfo.Def = tmp.def;
            PInfo.HpRec = tmp.hprec;
            PInfo.EngRec = tmp.enrec;
            PInfo.Str = tmp.str;
            PInfo.Agi = tmp.agi;
            PInfo.Dge = tmp.dge;
            PInfo.Hit = tmp.hit;
            PInfo.Crt = tmp.crt;
            PInfo.Lck = tmp.lck;
            PInfo.Bwt = tmp.bwt;
            PInfo.xSize = tmp.xsize;
            PInfo.BaseExp = tmp.bxp;
            PInfo.Memory = tmp.mem;
            PInfo.cStr = tmp.str;
            //*
            //*
            if (PInfo.Movepool==null){
                PInfo.Movepool = tmp.getMovesAtLv(1, PInfo.level);
            }
            PInfo.generated = true;
            //*/
        }
    }

    public void Spawn(PieceInfo PInfo,int x, int y){ //Spawn an existing PieceInfo
        if (!PInfo.isActive){
            PInfo.isActive = true;
            GameObject gmob = Instantiate(GR.basePiece, new Vector3(x, 0, y), Quaternion.Euler(0, 0, 0));
            GameObject gfx = gmob.transform.GetChild(0).gameObject;
            //Creates Graphix
            GameObject mdl = Instantiate(GR.characterModels[PInfo.species],
                                         gfx.transform.position,
                                         gfx.transform.rotation,
                                         gfx.transform);
            gmob.GetComponent<BattleMechanics>().SetInfo(PInfo);
            SpriteRenderer sr = gmob.transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>();
            sr.color = GR.teamColors[PInfo.team];
            //Change team indicator:
            //...
            //Change Stats:

        }
    }
}
