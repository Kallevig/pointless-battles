﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackBoxBehaviour : MonoBehaviour {
    public GameObject Master;
    int Size;
    public float H;
    float tmpH;
    public GameObject Buttons;
    List<GameObject> obs;
	// Use this for initialization
	void Start () {
        Size = 0;
        tmpH = 0;
        obs = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
        UpdateSize();
	}
    void UpdateSize()
    {
        Transform tmpParent = transform.GetChild(0).GetChild(0);
        BattleMechanics btl = Master.GetComponent<BattleMechanics>();
        AttackButtonBehaviour tmp2;
        while ((Size < btl.MovePool.Length) && (btl.MovePool[Size]!=0))
        {
            //GameObject gmOb = Instantiate(Buttons, Vector3.zero, Quaternion.Euler(0, 0, 0), tmpParent);
            GameObject gmOb = Instantiate(Buttons, tmpParent, false);
            gmOb.transform.localPosition = new Vector3(0, 1, 0) * tmpH;
            tmp2 = gmOb.GetComponent<AttackButtonBehaviour>();
            tmp2.Master = Master;
            tmp2.index = Size;
            tmpH += H;
            obs.Add(gmOb);
            Size += 1;
        }
    }
}
