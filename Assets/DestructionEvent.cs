﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructionEvent : MonoBehaviour {
    public GameObject Spawn;
    // Use this for initialization
    private void OnDestroy()
    {
        Instantiate(Spawn,transform.position,Quaternion.Euler(0,0,0));
    }
}
