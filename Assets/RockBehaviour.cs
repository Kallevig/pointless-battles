﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockBehaviour : MonoBehaviour {

    WorldMap mp;
    public bool CanCutCorners;
    public Management mng;
	// Use this for initialization
    //Obstacles cant cut, blocks can, lissom.
	void Start () {
        mng = GameObject.Find("_manager").GetComponent<Management>();
        mp = mng.OverWorld;
        if (CanCutCorners)
        {
            mp.PlaceObstacle(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.z));
            mng.PlaceBlock(transform.position);
        }
        else
        {
            mp.PlaceBlock(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.z));
            mng.PlaceObstacle(transform.position);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
