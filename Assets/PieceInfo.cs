﻿//PieceInfo contains all the information neccesary to store and create a
//piece.
//Example:
//The Management function CreatePiece(PieceInfo, int, int)
//uses this object to create a game piece on the map.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//NB: The Management class can generate a compleate PieceInfo.
public class PieceInfo
{
    //General:
    public string name;
    public int    level;
    public string description;       //Editable background and story, but not to the extent as customDescription.
    public string custonDescription; //used for to write stories and background about the character or enter tags to filter characters.
    public int    fav;               //used to favorize characters. Default: 1.
    public int    species;           //id of what species it is.
    public int    team;
    public List<int> Alliances;
    //Base Stats: Base stats:
    public bool generated; //this is false for PieceInfos that not yet has been generated and is set to true once they are.
    public int HP, hp, Atk, Def;   //  list will not attack you.
    public int HpRec, EngRec;
    public int cStr; //Current energy / strength.
    public int Str, Agi, Dge, Hit, Crt, Lck;
    public int Bwt; //Base weight
    public int xSize;        //This increases the size of the piece.
    public int BaseExp;
    public int Memory; //Increases the amount of moves a piece can now at once.
    //Genetics: Fixed values that can not be changed.
    public int gHP, gAtk, gDef;   //  list will not attack you.
    public int gHpRec, gEngRec;
    public int gStr, gAgi, gDge, gHit, gCrt, gLck;
    public int gBwt; //Base weight
    public int gMemory;
    //Training: 
    public int tHP, tAtk, tDef;   //  list will not attack you.
    public int tHpRec, tEngRec;
    public int tStr, tAgi, tDge, tHit, tCrt, tLck;
    public int tBwt; //Base weight
    public int curXp; //Current collected experience out of nextXp.
    public int nextXp; //Amount of experience needed to reach the next level.
    public int tMemory;
    public bool isActive;
    //Moves:
    public List<int> Movepool; //Moved indexes, where 0 is an empty move and a "terminator".
    //Terminator means any move after the first 0 is not counted as moves in the move pool.

    //Misc:
    public int  radius; //The radius where pieces inside this range can be seen and attacked.
    public bool aggressive; //Whether the piece will attack pieces or not.
    public bool followLeader; //Whether the piece will follow the leader or not.
    public int  stratergy;    //Stratergy index. Eg: 0: Attack everything, 1: Be passive. 2: Run away. 3: Prioritize defensive moves.
    public int genLevel;   //Lower level will autogenerate worse movepools and stratergies, and higher level will do better.

    public PieceInfo(int _species, int lv = 5, List<int> mpool = null, bool randomGenetics = true, int geneticRadius = 16, int genLv = 1){
        species = _species;
        generated = false;
        isActive = false;
        name = "";
        level = lv;
        description = "";
        custonDescription = "";
        fav = 1;
        team = 1; //Enemy by default;
        Alliances = new List<int>();
        Alliances.Add(team);
        //Base stats are temporary set to 1 when a piece is not geretated:
        HP = 1; hp = 1; Atk = 1; Def = 1; HpRec = 1; EngRec = 1; Str = 1; Agi = 1;
        Dge = 1; Hit = 1; Crt = 1; Lck = 1; Bwt = 1; xSize = 0; BaseExp = 1; Memory = 1;
        cStr = 1;

        tHP = 0; tAtk = 0; tDef = 0; tHpRec = 0; tEngRec = 0; tStr = 0; tAgi = 0;
        tDge = 0; tHit = 0; tCrt = 0; tLck = 0; tBwt = 0; tMemory = 0;

        if (randomGenetics){
            gHP = - geneticRadius +  Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gAgi = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gAtk = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gBwt = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gCrt = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gDef = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gDge = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gHit = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gLck = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gStr = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gHpRec = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gEngRec = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
            gMemory = -geneticRadius + Mathf.RoundToInt(Random.Range(0, geneticRadius));
        }
        else{
            gHP = 0;
            gAgi = 0;
            gAtk = 0;
            gBwt = 0;
            gCrt = 0;
            gDef = 0;
            gDge = 0;
            gHit = 0;
            gLck = 0;
            gStr = 0;
            gHpRec = 0;
            gEngRec = 0;
            gMemory = 0;
        }
        Movepool = mpool;
        //If Movepool = null, it will be automatically generated.
        genLevel = genLv;
    }

    public void setTeam(int tm){
        team = tm; //Enemy by default;
        Alliances = new List<int>
        {
            tm
        };
    }
}
