﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileBehaviour : MonoBehaviour {
    public GameObject Master;
    public int Distance;
    public int Direction;
    public int Power;
    public float Speed;
    public Vector3 p0;
    Vector3 p1;
    MoveInfo Info;
    int steps,step;
    bool Crit;
    float mod;
    int delay;
    public GameObject HitEffect;
    EventHandeling EH;
    Transform Tr;
    GameResources GR;
	// Use this for initialization
	void Awake () {
        Tr = GetComponent<Transform>();
        step = 0;
        EH = GameObject.Find("_manager").GetComponent<EventHandeling>();
        Info = GetComponent<MoveInfo>();
        Crit = false;
        GR = GameObject.Find("_manager").GetComponent<GameResources>();

    }
    public void SetCourse()
    {
        p0 = Tr.position;
        p1 = p0 + new Vector3(Mathz.Coz(Direction), 0, Mathz.Zin(Direction))*Info.Info.Distance;
        transform.rotation = Quaternion.Euler(0, -Direction * 45, 0);
        float tmpDist = Vector3.Distance(p0, p1);
        steps = Mathf.RoundToInt(tmpDist / Info.Info.MoveSpd);
        Crit = Mathz.Percent(Info.Info.CritChance);
        delay = Info.Info.Delay;
        mod = 1f;
        if (Crit) { mod *= 2; }
    }
    private void OnTriggerEnter(Collider other)
    {
        //Debug.Log(other.gameObject.name);
        if (other.gameObject.tag == "BattlePiece")
        {
            BattleMechanics otr = other.gameObject.GetComponent<BattleMechanics>();
            if ((otr != Info.Master) && (!Info.Master.IsFriendly(otr)))
            {
                //Damage:
                int tmpDmg = Mathz.Damage(Info.Info.Power, Info.Master.Atk, otr.Def, 0.1f, mod);
                //otr.hp -= tmpDmg;
                if (tmpDmg != 0) { otr.TakeDamage(tmpDmg); }
                if (Info.Info.ExEffect!=null) { otr.AddStatusEffect(Info.Info.ExEffect, Info.Master, mod); }
                BattleMechanics btl = Master.GetComponent<BattleMechanics>();
                otr.agro.AddAgro(Master, 0.0001f * tmpDmg * tmpDmg * btl.bSpd * btl.bSpd);
                otr.UpdateTarget();
                if (Info.Info.BlastIndex!=0)
                {
                    GameObject tmp2 =
                        Instantiate(GR.MoveAnimations[Info.Info.BlastIndex],
                                    new Vector3(otr.X+0.5f,0.5f,otr.Y+0.5f),
                                    Quaternion.Euler(0,0,0));
                }
                Destroy(gameObject);
            }
        }
    }

    // Update is called once per frame
    void Update () {
        if (delay > 0)
        {
            delay--;
        }
        else
        {
            step += 1;
            Tr.position = p0 + (step * (p1 - p0) / steps);
            if (step >= steps)
            {
                if (Info.Info.BlastIndex != 0)
                {
                    GameObject tmp2 =
                        Instantiate(GR.MoveAnimations[Info.Info.BlastIndex],
                                    new Vector3(Mathf.FloorToInt(Tr.position.x)+0.5f, 0.5f,Mathf.FloorToInt(Tr.position.z)+0.5f),
                                    Quaternion.Euler(0, 0, 0));
                }
                Destroy(gameObject);
            }
        }
        EH.Prolong();
    }
}
