﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClockBehaviour : MonoBehaviour {
    Management mng;
    RectTransform m, h, s;
	// Use this for initialization
	void Start () {
        mng = GameObject.Find("_manager").GetComponent<Management>();
        m = transform.GetChild(1).GetComponent<RectTransform>();
        h = transform.GetChild(2).GetComponent<RectTransform>();
        s = transform.GetChild(3).GetComponent<RectTransform>();
    }
	
	// Update is called once per frame
	void Update () {
        s.rotation = Quaternion.Euler(0, 0, 90-(mng.WorldTime * 360f / 60));
        m.rotation = Quaternion.Euler(0, 0, 90-(mng.WorldTime * 360f / 3600));
        h.rotation = Quaternion.Euler(0, 0, 90-(mng.WorldTime * 360f / 43200));

    }
}
