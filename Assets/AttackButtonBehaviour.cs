﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackButtonBehaviour : MonoBehaviour {
    public GameObject Master;
    public int index;
    Text txt;
    GameResources GR;

	// Use this for initialization
	void Start () {
        txt = transform.GetChild(0).GetChild(0).gameObject.GetComponent<Text>();
        GR = GameObject.Find("_manager").GetComponent<GameResources>();
	}
    void Update()
    {
        Master = transform.parent.parent.parent.gameObject.GetComponent<AttackBoxBehaviour>().Master;
    }
    // Update is called once per frame
    void LateUpdate()
    {
        BattleMechanics btl = Master.GetComponent<BattleMechanics>();
        int usMv = btl.MovePool[index];
        if (usMv!=0)
        {
            txt.text = GR.Attacks[usMv].Name;
        }
        else
        {
            txt.text = "---";
        }
    }
    public void AttackButtonClick()
    {
        BattleMechanics btl = Master.GetComponent<BattleMechanics>();
        EventHandeling EH = GameObject.Find("_manager").GetComponent<EventHandeling>();
        if (EH.IsMyTurn())
        {
            EH.ProceedPlease(
                20000 + btl.Direction * 1000 + index
            );
        }

    }
}
