﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mathz
{
    public static int Coz(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Cos(_dir * Mathf.PI / 4)));
    }
    public static int Zin(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Sin(_dir * Mathf.PI / 4)));
    }
    public static bool Percent(float pr)
    {
        float tmp = Random.Range(0, 100);
        return (tmp < pr);
    }
    public static int XpFormula(int lv)
    {
        if (lv==1)
        {
            return (8);
        }
        else
        {
            int tmp = lv + 1;
            return ((tmp * tmp * tmp) - (lv * lv * lv));
        }
    }
    public static int Range(int i0, int i1)
    {
        return Mathf.RoundToInt(Random.Range(i0, i1));
    }
    public static int Directify(int _dir)
    {
        int tempDir = _dir;
        while (tempDir < 0) { tempDir += 8; }
        while (tempDir >= 8) { tempDir -= 8; }
        return tempDir;
    }
    public static int Damage(int _pwr, int _atk, int _def)
    {
        return Mathf.CeilToInt(0.01f*_pwr*_atk*_atk/_def);
    }
    public static int Damage(int _pwr, int _atk, int _def,float variation,float mod)
    {
        float _vrt = 1f - variation + Random.Range(0, 2 * variation);
        return Mathf.CeilToInt(0.01f * mod * _pwr * _atk * _atk*_vrt / _def);
    }
    public static int CostFunc(int baseCost,int lv)
    {
        return (Mathf.CeilToInt(0.01f*baseCost * lv));
    }
    public static int PointDirection(Coord p0, Coord p1)
    {
        int TempDir = 0;
        float dx = p1.x - p0.x;
        float dy = p1.y - p0.y;
        float rr = Mathf.Sqrt(dx * dx + dy * dy);
        if (dy > 0)
        {
            TempDir = Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else if (dy < 0)
        {
            TempDir = 8 - Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else if (rr > 0)
        {
            TempDir = Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else
        {
            Debug.LogError("You attempted to check the difference between the same coords");
        }

        return TempDir;

    }

    public static int PointDirection(int x0, int y0, int x1, int y1)
    {
        int TempDir = 0;
        float dx = x1 - x0;
        float dy = y1 - y0;
        float rr = Mathf.Sqrt(dx * dx + dy * dy);
        if (dy > 0)
        {
            TempDir = Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else if (dy < 0)
        {
            TempDir = 8 - Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else if (rr > 0)
        {
            TempDir = Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else
        {
            Debug.LogError("You attempted to check the difference between the same coords");
        }

        return TempDir;

    }
}
