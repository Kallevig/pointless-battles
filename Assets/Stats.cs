﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stats : MonoBehaviour {

    public int team;  //This is the index of the team.
    public List<int> alliances;     //Pieces with team index that is in this
    public int HP, hp, Atk, Def;   //  list will not attack you.
    public int Str, Agi, Dge, Hit, Crt, Lck;
    public int HpRec, EngRec; //Recovery variables.
    public int Bwt; //Base weight.
    public int BaseExp;
    public int Lv;
    public int memory;
	// Use this for initialization
	void Start () {
        hp = HP;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public int StatFormula(int bStat,int lv,float _mod, int _add, float _g) //genetics.
    {
        float tmpMod = 1;
        if( _mod+_g<0)
        {
            tmpMod = 1f / (1 - ((_mod + _g) * 0.01f));
        }
        else
        {
            tmpMod = 1f * (1 + ((_mod + _g) * 0.01f));
        }
        return Mathf.CeilToInt(
            ((lv * 2f * bStat / 100) + (1f * bStat / 20) + _add) * tmpMod
       );
    }
}
