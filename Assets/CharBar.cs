﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharBar : MonoBehaviour {
    public GameObject Master;
    BarBehaviour BB;
    GameObject Child;
	// Use this for initialization
	void Start () {
        Child = transform.GetChild(0).gameObject;
        BB = Child.GetComponent<BarBehaviour>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Master!=null)
        {
            Child.SetActive(true);
            BB.Master = Master;
        }
        else
        {
            Child.SetActive(false);
        }
	}
}
