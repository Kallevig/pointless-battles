﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Decisions:
 * 1xxx Movement
 * 2xxx Use Move
 * 3xxx Use Item
 */

public class BattleMechanics : MonoBehaviour {
    public string Name;
    public int Decision,DDir, Direction,OldDecision; //DDir is decision direction.
    public BattleMechanics Swap, Swapper;
    public bool Remote;
    public Path myPath;
    public List<Coord> walkPath;
    public int SelfAnim,SelfDir;
    public float curSpd;
    public int bSpd;
    public int X, Y;
    public float x, y;
    public int CurAction;
    public bool Walk;
    public bool PrintCurSpd;
    public int[] MovePool;
    public int XSize;
    public int animFrame;
    public int Radius;
    public int memory;
    PieceInfo myInfo;
    public List<StatusEffect> myEffects;
    NavMap Navigation;
    public GameObject Target;
    public GameObject Following;
    EventHandeling EH;
    GameResources GR;
    Transform tr;
    Transform gTr;
    Stats stats;
    Genetics Gn;
    Animator CharAnim;
    List<BattleMechanics> ExternEnemies;
    public bool Invincible;
    public Agro agro;

    public int HP, Atk, Def;
    public int HpRec, EngRec;
    public int Str, Agi, Dge, Hit, Crt, Lck;
    public int Bwt; //Base weight.
    public int hp, Lv, curXp, NextXp;
    public int cStr;
    bool Dead;
    public bool Aggressive;
    float recHp;
    float recEg;
    float tempTime;
    public bool isWanderer;
    Management mg;
    // Use this for initialization
    void Awake () {
        Decision = 0;
        Direction = 0;
        DDir = -1;
        walkPath = new List<Coord>();
        SelfAnim = 0;
        SelfDir = 0;
        curSpd = 0f;
        Walk = false;
        myInfo = null;
        stats = GetComponent<Stats>();
        Gn = GetComponent<Genetics>();
    }
    void Start()
    {
        mg = GameObject.Find("_manager").GetComponent<Management>();
        myEffects = new List<StatusEffect>();
        animFrame = 0;
        tr = GetComponent<Transform>();
        gTr = transform.GetChild(0).GetComponent<Transform>();
        Navigation = mg.Navigator;
        CharAnim = transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).GetComponent<Animator>();
        X = TrX();
        Y = TrY();
        EH = GameObject.Find("_manager").GetComponent<EventHandeling>();
        GR = GameObject.Find("_manager").GetComponent<GameResources>();
        agro = GetComponent<Agro>();
        ExternEnemies = new List<BattleMechanics>();
        walkPath.Add(new Coord(X,Y));
        Swap = null;
        Swapper = null;
        if (XSize>0)
        {
            EH.SetPMap(X, Y, XSize, true);
        }
        else
        {
            EH.PMap[X, Y] = true;
        }
        Lv = stats.Lv;
        UpdateStats();
        hp = HP;
        cStr = Str;
        Dead = false;
        recHp = 0;
        recEg = 0;
        tempTime = mg.WorldTime;
    }
    bool Alligned(BattleMechanics btl,int Length)
    {
        int tmpdir = Mathz.PointDirection(X, Y, btl.X, btl.Y);
        int tmpX = X;
        int tmpY = Y;
        for (int i = 1; i <= Length; i++)
        {
            tmpX = X + Mathz.Coz(tmpdir) * i;
            tmpY = Y + Mathz.Zin(tmpdir) * i;
            if (!EH.IsFreeSpace(tmpX, tmpY)) { break; }
        }
        return (btl.X == tmpX && btl.Y == tmpY);
    }
    int DistanceTo(BattleMechanics btl, int Length)
    {
        int tmpdir = Mathz.PointDirection(X, Y, btl.X, btl.Y);
        int tmpX = X;
        int tmpY = Y;
        int tmpDist = 0;
        for (int i = 1; i <= Length; i++)
        {
            tmpX = X + Mathz.Coz(tmpdir) * i;
            tmpY = Y + Mathz.Zin(tmpdir) * i;
            tmpDist += 1;
            if (!EH.IsFreeSpace(tmpX, tmpY)) { break; }
        }
        return (tmpDist);
    }
    float Distance(BattleMechanics btl)
    {
        if (btl != null)
        {
            float xxx = X - btl.X;
            float yyy = Y - btl.Y;
            return (Mathf.Sqrt(xxx * xxx + yyy * yyy));
        }
        else
        {
            return (0);
        }
    }
    int ReachableMove(BattleMechanics btl)
    {
        int tmp = -1;
        List<int> tmpList = new List<int>();
        Attack tmpAtk;
        if (Alligned(btl,Radius))
        {
            for (int i = 0; i < MovePool.Length; i++)
            {
                if (MovePool[i]==0)
                {
                    break;
                }
                tmpAtk = GR.Attacks[MovePool[i]];
                if (tmpAtk.Distance >= DistanceTo(btl, Radius))
                {
                    tmpList.Add(i);
                }
            }
        }
        if (tmpList.Count > 0)
        {
            tmp = Mathf.FloorToInt(Random.Range(0, tmpList.Count));
            return tmpList[tmp];
        }
        else
            return tmp;
    }
    List<BattleMechanics> GetReachable()
    {
        List<BattleMechanics> tmpList = new List<BattleMechanics>();
        GameObject[] pics = GameObject.FindGameObjectsWithTag("BattlePiece");
        BattleMechanics tmp;
        foreach (GameObject gmob in pics)
        {
            tmp = gmob.GetComponent<BattleMechanics>();
            if (!IsFriendly(tmp) && (ReachableMove(tmp)!=-1))
            {
                tmpList.Add(tmp);
            }
        }
        return tmpList;
    }
    void GetDecisionAI()
    {
        //Debug.Log(name+" is deciding...");
        Decision = 1999; //Default is pass.
        List <BattleMechanics> Reachable = GetReachable(); //Candidate 1.
        if (Aggressive && Target==null)
        {
            //GetATarget:
            if (Reachable.Count > 0)
            {
                int tmp0 = Mathf.FloorToInt(Random.Range(0, Reachable.Count));
                Target = Reachable[tmp0].gameObject;
                agro.AddAgro(Target, 1);
            }
            else
            {
                Target = GetClosestEnemy(Radius);
                agro.AddAgro(Target, 1);
            }
        }
        if (Target!=null)
        {
            BattleMechanics btl;
            btl = Target.GetComponent<BattleMechanics>();
            while ((Distance(btl) > Radius) && (Target!=null))
            {
                agro.Delete(Target);
                UpdateTarget();
                if (Target != null)
                {
                    btl = Target.GetComponent<BattleMechanics>();
                }
                else
                {
                    btl = null;
                }
            }
            if (btl!=null)
            {
                int tmpInt = ReachableMove(btl);
                if (tmpInt!=-1)
                {
                    Decision = 20000 + Mathz.PointDirection(X, Y, btl.X, btl.Y)*1000 + tmpInt;
                }
                else
                {
                    if(Mathf.FloorToInt(Distance(btl))>1)
                    {
                        GetDecisionGoto(btl.X, btl.Y);
                    }
                }
            }
        }
        if (Target==null)
        {
            if (Following!=null)
            {
                GetDecisionFollow();
            }
        }
        if (Decision==1999 && isWanderer){
            Wander();
        }
    }
    public void Wander(float p=95){
        int tmp3 = 1999;
        List<int> forw = new List<int>();
        List<int> backw = new List<int>();

        for (int i = 0; i < 5; i++){
            int dd = Mathz.Directify(Direction - 2 + i);
            if (EH.Traversable(X,Y,X+Mathz.Coz(dd),Y+Mathz.Zin(dd),this)){
                forw.Add(1000 + dd);
            }
        }

        for (int i = 0; i < 3; i++)
        {
            int dd = Mathz.Directify(Direction - 5 + i);
            if (EH.Traversable(X, Y, X + Mathz.Coz(dd), Y + Mathz.Zin(dd), this))
            {
                backw.Add(1000 + dd);
            }
        }

        if (Mathz.Percent(p) && forw.Count > 0){
            int i2 = Mathf.FloorToInt( Random.Range(0,forw.Count));
            tmp3 = forw[i2];
        }else{
            if (backw.Count > 0)
            {
                int i2 = Mathf.FloorToInt(Random.Range(0, backw.Count));
                tmp3 = backw[i2];
            }
        }
        Decision = tmp3;

    }
    public void Wander2(float p=50)
    {
        int tmp3 = 1999;
        int dirat = 3;
        for (int i = dirat; i >= 0; i--){

            if(Mathz.Percent(50)){
                if(Navigation.Traversable(X,Y,Direction+i)){
                    tmp3 = 1000 + Mathz.Directify( Direction + i);
                    break;
                }else{
                    if (Navigation.Traversable(X, Y, Direction - i))
                    {
                        tmp3 = 1000+Mathz.Directify(Direction - i);
                        break;
                    }
                }
            }else{
                if (Navigation.Traversable(X, Y, Direction -i))
                {
                    tmp3 = 1000+Mathz.Directify( Direction -i );
                    break;
                }else{
                    if (Navigation.Traversable(X, Y, Direction + i))
                    {
                        tmp3 = 1000 + Mathz.Directify( Direction + i );
                        break;
                    }
                }
            }
        }
        if (tmp3 == 1999 && Navigation.Traversable(X, Y, Direction + 4)) { tmp3 = 1000 + Mathz.Directify( Direction + 4); }
        Decision = tmp3;

    }
    GameObject GetClosestEnemy(float MaxRadius)
    {
        List<BattleMechanics> tmpList = new List<BattleMechanics>();
        GameObject[] pics = GameObject.FindGameObjectsWithTag("BattlePiece");
        GameObject tmp = null;
        float tmpd = MaxRadius;
        float tmpd2;
        BattleMechanics btl;
        if (pics.Length > 0)
        {
            foreach (GameObject gmob in pics)
            {
                btl = gmob.GetComponent<BattleMechanics>();
                tmpd2 = Distance(btl);
                if (!IsFriendly(btl) && (tmpd2<tmpd))
                {
                    tmp = gmob;
                    tmpd = tmpd2;
                }
                else if (IsFriendly(btl)){
                    //Debug.Log(name+" thinks " + btl.name + " is friendly :)");
                }
            }
        }
        return tmp;
    }

    public void SwapRequest(int x0, int y0)
    {
        if (Remote)
        {
            //Debug.Log("1");
            GameObject[] pics = GameObject.FindGameObjectsWithTag("BattlePiece");
            BattleMechanics _btl, _btl2;
            _btl2 = null;
            for (int i = 0; i < pics.Length; i++)
            {
                _btl = pics[i].GetComponent<BattleMechanics>();
                if ((_btl.X == x0) && (_btl.Y == y0))
                {
                    //Debug.Log("2");
                    _btl2 = _btl;
                    break;
                }
            }
            if (_btl2 != null && (_btl2.XSize == XSize) && IsFriendly(_btl2))
            {
                _btl2.Swap = this;
                Swapper = _btl2;
            }
        }
    }
    public void GetDecisionSwap()
    {
        int tmp = Mathz.PointDirection(X, Y, Swap.X, Swap.Y);
        Decision = 1000 + tmp;
    }
    public bool IsFriendly(BattleMechanics _btl)
    {
        List<int> _allies = _btl.stats.alliances; //This is a reference!
        bool tmp = false;
        if (!ExternEnemies.Contains(_btl))
        {
            if (stats.team == _btl.stats.team)
            {
                tmp = true;
            }
            else
            {
                for (int i = 0; i < _allies.Count; i++)
                {
                    if (_allies[i] == stats.team)
                    {
                        tmp = true;
                        break;
                    }
                }
            }
        }
        return tmp;
    }
    public void AddEnemy(BattleMechanics _btl)
    {
        ExternEnemies.Add(_btl);
    }
    public void UpdateStats()
    {
        //Moifiers and additions set to 0:
        float tHp, tAtk, tDef, tStr, tAgi, tDge, tHit, tCrt, tLck, tHpRec, tEngRec;
        int aHp, aAtk, aDef, aSpd, aStr, aAgi, aDge, aHit, aCrt, aLck, aHpRec, aEngRec;
        tHp = 0; tAtk = 0; tDef = 0; tStr = 0; tAgi = 0; tDge = 0; tHit = 0; tCrt = 0; tLck = 0;
        aHp = 0; aAtk = 0; aDef = 0; aStr = 0; aAgi = 0; aDge = 0; aHit = 0; aCrt = 0; aLck = 0;
        aHpRec = 0;tHpRec = 0;aEngRec = 0;tEngRec = 0;
        for (int i = 0; i < myEffects.Count; i++)
        {
            aHp += myEffects[i].aHP;
            aAtk += myEffects[i].aAtk;
            aDef += myEffects[i].aDef;
            aStr += myEffects[i].aStr;
            aAgi += myEffects[i].aAgi;
            aDge += myEffects[i].aDge;
            aHit += myEffects[i].aHit;
            aCrt += myEffects[i].aCrt;
            aLck += myEffects[i].aLck;
            aHpRec += myEffects[i].aHpRec;
            aEngRec += myEffects[i].aEngRec;

            tHp += myEffects[i].HP;
            tAtk += myEffects[i].Atk;
            tDef += myEffects[i].Def;
            tStr += myEffects[i].Str;
            tAgi += myEffects[i].Agi;
            tDge += myEffects[i].Dge;
            tHit += myEffects[i].Hit;
            tCrt += myEffects[i].Crt;
            tLck += myEffects[i].Lck;
            tHpRec += myEffects[i].HpRec;
            tEngRec += myEffects[i].EngRec;
        }
        //Updated modifiers.
        /*
        public int HP, Atk, Def, Spd;   //  list will not attack you.
        public int Str, Agi, Dge, Hit, Crt, Lck;
        public int Bwt; //Base weight.
        public int hp;
        */
        HP = stats.StatFormula(stats.HP, Lv, tHp, aHp, Gn.HP);
        Atk = stats.StatFormula(stats.Atk, Lv, tAtk, aAtk, Gn.Atk);
        Def = stats.StatFormula(stats.Def, Lv, tDef, aHp, Gn.Def);
        Str = stats.StatFormula(stats.Str, Lv, tStr, aHp, Gn.Str);
        Agi = stats.StatFormula(stats.Agi, Lv, tAgi, aHp, Gn.Agi);
        Dge = stats.StatFormula(stats.Dge, Lv, tDge, aHp, Gn.Dge);
        Hit = stats.StatFormula(stats.Hit, Lv, tHit, aHp, Gn.Hit);
        Crt = stats.StatFormula(stats.Crt, Lv, tCrt, aHp, Gn.Crt);
        Lck = stats.StatFormula(stats.Lck, Lv, tLck, aHp, Gn.Lck);
        Bwt = stats.Bwt;
        bSpd = Agi + Mathf.RoundToInt(1f * Str / (1+Bwt));
        HpRec = stats.StatFormula(stats.HpRec, Lv, tHpRec, aHpRec, Gn.HpRec);
        EngRec = stats.StatFormula(stats.EngRec, Lv, tEngRec, aEngRec, Gn.EngRec);
        NextXp = Mathz.XpFormula(Lv);
    }
    int TrX()
    {
        return Mathf.RoundToInt(tr.position.x);
    }
    int TrY()
    {
        return Mathf.RoundToInt(tr.position.z);
    }
    public void GetDecisionGoto(int _x, int _y)
    {
        List<Coord> pFl = Navigation.FindPathEx(X, Y, _x, _y);
        pFl.Add(new Coord(_x, _y));
        Decision = 1000 + Mathz.PointDirection(X, Y, pFl[1].x, pFl[1].y);
    }
    public void GetDecisionFollow()
    {
        BattleMechanics flwB = Following.GetComponent<BattleMechanics>();
        GetDecisionGoto(flwB.X, flwB.Y);
    }
    public void GetDecision()
    {
        if (Swap == null)
        {
            GetDecisionAI();

            //Modified:
            if (Mathz.Percent(0))
            {
                Decision = 20000;
                //Decision += Mathz.Range(0, 7) * 1000;
                Decision += 1000;
                Decision += 1; //Index of move.
            }
        }
        else
        {
            GetDecisionSwap();
        }
        ValidateDecision();
    }
    public void SetAnimationSpeed(float _spd)
    {
        CharAnim.SetFloat("AnimationSpeed", _spd);
    }
    public void Place(float _i)
    {
        myPath.SetPos(_i);
        transform.position = new Vector3(myPath.x, transform.position.y, myPath.y);
        Direction = myPath.Direction;
    }
    public void ResetPos()
    {
        tr.position = new Vector3( TrX(), tr.position.y, TrY() );
        walkPath.Clear();
        walkPath.Add(new Coord(TrX(), TrY()));
        Walk = false;

    }
    public void UpdateTarget()
    {
        Target = agro.GetAgroObject();
    }
    public void UseMove(int _i,int _dir)
    {
        Attack tmpMv = GR.Attacks[MovePool[_i]];
        if (!Dead) { CharAnim.SetTrigger("Attack"); }
        Direction = _dir; int _cost = Mathz.CostFunc(tmpMv.EnergyCost, Lv);
        cStr -= _cost;
        GameObject gmOb = Instantiate(GR.StandardMove, (tr.position + new Vector3(0.5f, 0.5f, 0.5f)), Quaternion.Euler(0, 0, 0));
        ProjectileBehaviour PB = gmOb.GetComponent<ProjectileBehaviour>();
        MoveInfo MI = gmOb.GetComponent<MoveInfo>();
        if (tmpMv.MoveIndex!=0){
            GameObject child1 = Instantiate(GR.MoveAnimations[tmpMv.MoveIndex],
                                            gmOb.transform.position,
                                            gmOb.transform.rotation, gmOb.transform);
        }
        MI.Info = tmpMv;
        MI.Master = this;
        PB.Direction = _dir;
        PB.Master = gameObject;
        PB.SetCourse();
    }
    public void ValidateDecision()
    {
        if ((Decision / 20000) == 1)
        {
            int _i = Decision % 1000;
            Attack tmpMv = GR.Attacks[MovePool[_i]];
            if(cStr<Mathz.CostFunc(tmpMv.EnergyCost,Lv))
            {
                Decision = 1999; //Force Pass!
            }
        }
    }
    public void TakeDamage(int _dmg){
        hp -= _dmg;
    }
    public bool ValidAttack(int _i)
    {
        bool tmp = true;
        Attack tmpMv = GR.Attacks[MovePool[_i]];
        if (cStr < Mathz.CostFunc(tmpMv.EnergyCost, Lv))
        {
            tmp=false; //Force Pass!
        }
        return tmp;
    }

    void CheckDeath(){
        if (hp < 0) { hp = 0; }
        if (hp > HP) { hp = HP; }
        if (cStr > Str) { cStr = Str; }
        if (cStr < 0) { cStr = 0; }
        if (hp == 0 && !Dead && !Invincible)
        {
            Dead = true;
            GameObject tmp2 = transform.GetChild(0).gameObject;
            tmp2.transform.parent = null;
            GameObject dexp = Instantiate(GR.MoveAnimations[3], new Vector3(X + 0.5f, 0.5f, Y + 0.5f), Quaternion.Euler(0, 0, 0));
            dexp.GetComponent<GrowthAnimation>().DOb = tmp2;
            EH.Prolong();
            EH.SetPMap(X, Y, XSize, false);
            Destroy(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Walk) { animFrame = 3; }
        if (animFrame > 0) { animFrame -= 1; }
        if (animFrame > 0)
        {
            CharAnim.SetInteger("AnimationIndex", 1);
        }
        else { CharAnim.SetInteger("AnimationIndex", 0); }
        CheckDeath();
    }
    void LateUpdate()
    {
        gTr.rotation = Quaternion.Euler(0,-(Direction*45)+90, 0);
    }

    public void OnMyTurn() //Called every time it is this pieces turn.
    {
        ;
    }
    public void EndMyTurn() //Called every time the turn ends for this piece.
    {
        ;
    }
    public void OnAction(float _dt){ //_dt is the time that has passed since last OnAction().

        //Recover:
        int addVal = 0;
        recHp += _dt * HpRec / 600; addVal = Mathf.FloorToInt(recHp); hp += addVal; recHp -= addVal;
        recEg += _dt * EngRec / 60; addVal = Mathf.FloorToInt(recEg); cStr += addVal; recEg -= addVal;

        //Changes in buffs:
        int iProceed = 1;
        for (int i = 0; i < myEffects.Count; i+=iProceed){
            iProceed = 1;
            myEffects[i].t0 += _dt;
            //Calculate damagae and heals:
            if (myEffects[i].DOT != 0)
            {
                int v0 = Mathf.RoundToInt(myEffects[i].DOT * myEffects[i].t0 / myEffects[i].t1);
                if (Mathf.Abs(v0) > Mathf.Abs(myEffects[i].DOT)) { v0 = myEffects[i].DOT; }
                int dmg = v0 - myEffects[i].DTK; myEffects[i].DTK = v0;
                TakeDamage(dmg);
            }
            if ( (myEffects[i].t0>=myEffects[i].t1) && myEffects[i].timeStop  ){
                myEffects.RemoveAt(i);
                iProceed = 0;
            }

        }
        //Check if dead:
        CheckDeath();
    }

    public int SEReplace(StatusEffect _SE){
        int tmp = -1;
        int j = 0;
        foreach (StatusEffect fx in myEffects){
            if( ((fx.Name==_SE.Name) && (fx.master==_SE.master)) && !_SE.stackable ){
                tmp = j;
                break;
            }
            j++;
        }
        return tmp;
    }

    public void AddStatusEffect(StatusEffect _SE,BattleMechanics _master,float _mod = 1f){
        StatusEffect F = _SE.clone();
        F.master = _master;
        //Modify F:
        if (F.Damage != 0)
        {
            F.DOT += Mathz.Damage(F.Damage, _master.Atk, Def, 0.1f, _mod);
        }
        //---------
        int tmp = SEReplace(F);
        if (tmp != -1)
        {
            myEffects.RemoveAt(tmp);
            Debug.Log("Removed 1 effect which was the same one.");
        }
        myEffects.Add(F);
    }

    public void SetInfo(PieceInfo PInfo){
        myInfo = PInfo;
        CopyStats();
    }
    public void CopyStats(){ //Copies stats from myInfo.
        //Stats stats = GetComponent<Stats>();
        //Genetics Gn = GetComponent<Genetics>();

        Name = myInfo.name;
        //Stats:
        stats.team = myInfo.team;
        stats.alliances = myInfo.Alliances;
        stats.HP = myInfo.HP;
        stats.hp = myInfo.HP;
        stats.Atk = myInfo.Atk;
        stats.Def = myInfo.Def;
        stats.Str = myInfo.Str;
        stats.Hit = myInfo.Hit;
        stats.Crt = myInfo.Crt;
        stats.Lck = myInfo.Lck;
        stats.HpRec = myInfo.HpRec;
        stats.EngRec = myInfo.EngRec;
        stats.Bwt = myInfo.Bwt;
        stats.BaseExp = myInfo.BaseExp;
        stats.Lv = myInfo.level;
        //Genetics:
        Gn.HP = myInfo.gHP;
        Gn.Atk = myInfo.gAtk;
        Gn.Def = myInfo.gDef;
        Gn.HpRec = myInfo.HpRec;
        Gn.EngRec = myInfo.EngRec;
        Gn.Str = myInfo.Str;
        Gn.Agi = myInfo.Agi;
        Gn.Dge = myInfo.Dge;
        Gn.Hit = myInfo.Hit;
        Gn.Crt = myInfo.Crt;
        Gn.Lck = myInfo.Lck;
        Gn.Bwt = myInfo.Bwt;
        hp = myInfo.hp;
        cStr = myInfo.cStr;
        NextXp = myInfo.nextXp;

    }
    public void SyncInfo(){ //Copies the stats into the myInfo.
        ;
    }
}
