﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 *Elements:
 *  Physical
 *  Water
 *  Fire
 *  Ice
 *  Nature
 *  Dark
 *  Holy
 *  Earth
*/

public class StatusEffect
{
    public float t0, t1; //if t0 > t1 and t1 > 0, stop doing stuff.
    public bool timeStop; //The buff deletes it selft when the t0>=t1.
    public float t2; //if t2>t0, the effect does nothing.
    public int AnimationEffectIndex, Damage, DeltaTime,Time;
    public string Name, description;
    public bool IsBuff;
    public bool stackable;
    public int Type;
    public int Effect; //0: none; 1: sleep; 2: Paralyzed; 3: Frozen; 4: Stunned.
    public int ShieldDmg;
    public float Absorb; //Absorbs dmg*Absorb<0,1+> into health for as long as ShieldDmg>0;
    public int DOT, DTK; //DOT = Damage over time. DTK: Damage taken.
    //Modifiers (a+b)*(1+m) or if (-) -> (a+b)*(1/(1-m))
    public float HP, Atk, Def, Spd;   //  list will not attack you.
    public float HpRec, EngRec;
    public float Str, Agi, Dge, Hit, Crt, Lck;
    public float Bwt; //Base weight.
    public float hp;
    public float heal; //Modifies healing reccieved.
    public float dmg; //Modifies the damage taken.
    //Additional stat.
    public int aHP, aAtk, aDef, aSpd;   //  list will not attack you.
    public int aHpRec, aEngRec;
    public int aStr, aAgi, aDge, aHit, aCrt, aLck;
    public int aBwt; //Base weight.
    public int ahp;
    public bool isVisible;
    public BattleMechanics master;
    public StatusEffect(string _name)
    {
        t0 = 0;
        t1 = 0;
        timeStop = true;
        t2 = -1;
        DOT = 0;
        DTK = 0;
        AnimationEffectIndex = 0;
        Damage = 0;
        DeltaTime = 0;
        Time = 0;
        Name = _name;
        description = "";
        IsBuff = false;
        Type = 0;
        Effect = 0;
        ShieldDmg = 0;
        Absorb = 0;
        dmg = 0;
        heal = 0;
        stackable = false;

        HP = 0;
        HpRec = 0;
        EngRec = 0;
        Atk = 0;
        Def = 0;
        Spd = 0;
        Str = 0;
        Agi = 0;
        Dge = 0;
        Hit = 0;
        Crt = 0;
        Lck = 0;
        Bwt = 0;
        hp = 0;

        aHP = 0;
        aHpRec = 0;
        aEngRec = 0;
        aAtk = 0;
        aDef = 0;
        aSpd = 0;
        aStr = 0;
        aAgi = 0;
        aDge = 0;
        aHit = 0;
        aCrt = 0;
        aLck = 0;
        aBwt = 0;
        ahp = 0;

        isVisible = false;
        master = null;
    }
    public string AutomaticDescription()
    {
        return "";
    }
    public StatusEffect clone(){
        StatusEffect st = new StatusEffect(this.Name);
        st.t0 = this.t0;
        st.t1 = this.t1;
        st.timeStop = this.timeStop;
        st.t2 = this.t2;
        st.DOT = this.DOT;
        st.DTK = this.DTK;
        st.AnimationEffectIndex = this.AnimationEffectIndex;
        st.Damage = this.Damage;
        st.DeltaTime = this.DeltaTime;
        st.Time = this.Time;
        st.description = this.description;
        st.IsBuff = this.IsBuff;
        st.Type = this.Type;
        st.Effect = this.Effect;
        st.ShieldDmg = this.ShieldDmg;
        st.Absorb = this.Absorb;
        st.dmg = this.dmg;
        st.heal = this.heal;
        st.stackable = this.stackable;

        st.HP = this.HP;
        st.HpRec = this.HpRec;
        st.EngRec = this.EngRec;
        st.Atk = this.Atk;
        st.Def = this.Def;
        st.Spd = this.Spd;
        st.Str = this.Str;
        st.Agi = this.Agi;
        st.Dge = this.Dge;
        st.Hit = this.Hit;
        st.Crt = this.Crt;
        st.Lck = this.Lck;
        st.Bwt = this.Bwt;
        st.hp = this.hp;

        st.aHP = this.aHP;
        st.aHpRec = this.aHpRec;
        st.aEngRec = this.aEngRec;
        st.aAtk = this.aAtk;
        st.aDef = this.aDef;
        st.aSpd = this.aSpd;
        st.aStr = this.aStr;
        st.aAgi = this.aAgi;
        st.aDge = this.aDge;
        st.aHit = this.aHit;
        st.aCrt = this.aCrt;
        st.aLck = this.aLck;
        st.aBwt = this.aBwt;
        st.ahp = this.ahp;
        st.isVisible = this.isVisible;
        st.master = this.master;
        return st;
    }
}

public class SEHolder //Status Effect Holder. Holds a status effect and timer.
{
    StatusEffect stat;
    float t;
    SEHolder(StatusEffect _SE){
        stat = _SE;
        t = 0;
    }
}

//Dmg = (atk*atkmod)**2/(def * (defmod**2));
//Heal = heal * mod

public class Attack
{
    public int selfIndex;
    public int Power, HitChance, CritChance, MoveIndex, BlastIndex, Cooldown, EnergyCost, Distance;
    public int BlastSize, Type, GlobalCooldown, Recharge, Heal, Charge;
    public int AnimationType; //0: None, 1: Normal Attack. 2: Ranged attack.
    public int EffectIndex; //0: None (Applies to all targets hit.
    public bool FriendlyFire; //Whether to hit allies as well.
    public bool Ally; //Whether this move is meant for an ally.
    public int AttackClass; //0: Front; 1: Projectile; 2: Area; 3: Status
    //4: Raycast Beam; 5: Piercing Beam
    public float Absorb; //Amount of damage dealt also converted to own health.
    public float MoveSpd;
    public string Name, description;
    public int Delay;
    public bool Visible;

    public float DOTPercent;
    public bool DOT;
    public float DOTChance;

    public StatusEffect ExEffect;

    public Attack(string _name)
    {
        selfIndex = 0;
        Name = _name;
        description = "";
        Power = 20;
        HitChance = 100;
        CritChance = 5;
        MoveIndex = 0;
        BlastIndex = 0;
        Cooldown = 0;
        GlobalCooldown = 0;
        EnergyCost = 10;
        Distance = 1;
        BlastSize = 0;
        Type = 1;
        Heal = 0;
        Recharge = 0;
        Charge = 0;
        AnimationType = 1;
        EffectIndex = 0;
        FriendlyFire = false;
        Ally = false;
        AttackClass = 1;
        Absorb = 0;
        MoveSpd = 0.2f;
        Visible = true;
        Delay = 24;

        DOTPercent = 20;
        DOT = false;
        DOTChance = 100;
        ExEffect = null;
    }
    public string AutoDescription(int _atk, int _def)
    {
        int tmpDmg = Mathz.Damage(Power, _atk, _def);
        string tmpStr = "";
        tmpStr += "Power: " + Power.ToString() + "\nAccuracy: " + HitChance.ToString() + "\n\n";
        tmpStr += "Deals around " + tmpDmg.ToString() + " damage to a target with similar defence stat.";
        return tmpStr;
    }
}

//Example Moves possible to make all ready: /Beams not possible yet
/*
 * Tackle
 * Cut
 * Fire Ball
 * Shadow Ball
 * Defence Buff
 * Health Buff
 * Poison Moves
 */