﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDestructTimer : MonoBehaviour {
    public float time;
    EventHandeling EH;
	// Use this for initialization
	void Start () {
        EH = GameObject.Find("_manager").GetComponent<EventHandeling>();

    }
	
	// Update is called once per frame
	void Update () {
        EH.Prolong();
        time -= Time.deltaTime;

        if (time <= 0) { Destroy(gameObject); }
	}
}
