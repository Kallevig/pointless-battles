﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameResources : MonoBehaviour {
    public GameObject[] Moves;
    public GameObject[] MoveAnimations;
    public GameObject StandardMove;
    public List<Attack> Attacks;
    public List<BaseInfo> BaseInfos;
    public List<StatusEffect> SEffects;
    public GameObject basePiece; //The gameObject used to spawn pieces.
    public GameObject[] characterModels;
    public Color[] teamColors;

	// Use this for initialization
	void Start () {
        Attacks = new List<Attack>();
        BaseInfos = new List<BaseInfo>(); //List over species.
        SetAttacks();
        SetBaseInfos();
	}
	
	// Update is called once per frame
	void Update () {
	}
    void SetAttacks()
    {
        Attack atk;
        StatusEffect fx;
        int II = 0;

        //0:
        atk = new Attack("");Attacks.Add(atk); atk.selfIndex = II; II++;
        atk.Name = "???";
        atk.Power = 1;

        //1:
        atk = new Attack(""); Attacks.Add(atk); atk.selfIndex = II; II++;
        atk.Name = "Strike";
        atk.Power = 10;
        atk.Distance = 1;
        atk.EnergyCost = 15;

        //2:
        atk = new Attack(""); Attacks.Add(atk); atk.selfIndex = II; II++;
        atk.Name = "Fire Blast";
        atk.MoveIndex = 1;
        atk.BlastIndex = 2;
        atk.Power = 20;
        atk.Distance = 4;
        atk.EnergyCost = 35;

        //3:
        atk = new Attack(""); Attacks.Add(atk); atk.selfIndex = II; II++;
        atk.Name = "Pyromaniac";
        atk.MoveIndex = 1;
        atk.BlastIndex = 2;
        atk.Power = 0;
        atk.Distance = 1;
        atk.EnergyCost = 50;
        fx = new StatusEffect("Heavy Burn");
        fx.Damage = 50; //Additional 15 power damage.
        fx.t1 = 60;
        fx.stackable = true;
        atk.ExEffect = fx;
    }
    void SetBaseInfos(){
        BaseInfo BI;
        int II = 0;

        //0
        BI = new BaseInfo(
            "???",
            II,/*
            hp  atk def str agi dge hit crt lck hrc erc bxp bwt mem */
            100,100,100,100,100,100,100,100,100,100,100,100,100,100
        ); BaseInfos.Add(BI); II++;
        BI.description =
              "???";
        BI.AddMove(1);

        //1
        BI = new BaseInfo(
            "Fuwanko",
            II,/*
            hp  atk def str agi dge hit crt lck hrc erc bxp bwt mem */
            25, 25, 25, 25, 100,25, 25, 25, 25, 25, 25, 25, 10, 25
        ); BaseInfos.Add(BI); II++;
        BI.description =
              "???";
        BI.AddMove(1); BI.AddMove(2,50);

        //2
        BI = new BaseInfo(
            "Pyradon",
            II,/*
            hp  atk def str agi dge hit crt lck hrc erc bxp bwt mem */
            100, 50, 100, 50, 50, 5, 50, 2, 25, 50, 25, 100, 500, 50
        ); BaseInfos.Add(BI); II++;
        BI.description =
              "Pyradon is strong in defence.";
        BI.AddMove(1); BI.AddMove(2, 5);

        //3
        BI = new BaseInfo(
            "Boushisan",
            II,/*
            hp  atk def str agi dge hit crt lck hrc erc bxp bwt mem */
            50, 50, 50, 50, 80, 150,50, 25, 25, 50, 50, 50, 5,  50
        ); BaseInfos.Add(BI); II++;
        BI.description =
              "???";
        BI.AddMove(1); BI.AddMove(2,20);

        //---------------------------------------------------------------
    }
}
