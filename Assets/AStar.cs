﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Coord
{
    public int x, y;
    public Coord( int xvar, int yvar )
    {
        x = xvar;
        y = yvar;
    }
    public bool Equals(Coord ccr)
    {
        return ((x == ccr.x) && (y == ccr.y));
    }
    public string ToStr()
    {
        return (x.ToString() + "x" + y.ToString());
    }

    public override string ToString()
    {
        return ToStr();
    }

    public float DistanceTo(Coord ccr)
    {
        float dx = 1f * (ccr.x - x);
        float dy = 1f * (ccr.y - y);
        return (Mathf.Sqrt(dx*dx+dy*dy));
    }
    public int UnitsTo(Coord ccr)
    {
        return Mathf.RoundToInt(10*DistanceTo(ccr));
    }
}

public class Node
{
    public Coord position, target;
    public int arrow; //Pointing to the position of parent node.
    public int G, H, F; //G = Path cost, H = Distance to target, F is sum.
    public Node Parent;
    public Node(Coord _position, Coord _target, int _G, int _arrow)
    {
        position = _position;
        target = _target;
        G = _G;
        arrow = _arrow;
        float dX = 1f * (target.x - position.x);
        float dY = 1f * (target.y - position.y);
        H = position.UnitsTo(target);
        F = G + H;

    }
    public Node(Coord _position, Coord _target, int _G, int _arrow,Node _parent)
    {
        position = _position;
        target = _target;
        G = _G;
        arrow = _arrow;
        float dX = 1f * (target.x - position.x);
        float dY = 1f * (target.y - position.y);
        H = position.UnitsTo(target);
        F = G + H;
        Parent = _parent;

    }
    public void SetParent(Node _parent)
    {
        Parent = _parent;
    }
    public void UpdateNode()
    {
        F = G + H;
    }
}
/*
 * World nodes are placed in the world map and remain there.
 * Unlike Nodes, they are both coordinates and a node at the same time.
 */
public class WorldNode
{
    public int x, y; //Coordinates of its own position.
    public int tx, ty; //Coordinates of its target.
    public int G, H, F; //G: Path cost, H: distance, F: sum;
    public int State; //0: is free, 1: is in OPEN list, 2: in CLOSED list.
    public WorldNode Parent;
    //Upon initialization, target position does not need to be set.
    //G, H, F, are also set later, and does not need to be initialized here.
    //Same goes for State and Parent.
    //This is because they will be set to a zerostate until they will be used
    //by a function. Therefore:
    public WorldNode(int _x, int _y)
    {
        x = _x;
        y = _y;
        Reset();
    }
    public void SetParent(WorldNode _parent)
    {
        Parent = _parent;
        G = Parent.UnitsToWorldNode(this);
        F = G + H;
    }
    public void SetParent(WorldNode _parent, float _mod)
    {
        Parent = _parent;
        G = Parent.UnitsToWorldNode(this,_mod);
        F = G + H;
    }
    public void Set(int _tx, int _ty, WorldNode _parent)
    {
        tx = _tx;
        ty = _ty;
        State = 1;
        SetParent(_parent);
        H = UnitsToTarget();
        F = G + H;
    }
    public void Set(int _tx, int _ty, WorldNode _parent,float _mod)
    {
        tx = _tx;
        ty = _ty;
        State = 1;
        SetParent(_parent,_mod);
        H = UnitsToTarget();
        F = G + H;
    }
    public void Set(int _tx, int _ty)
    {
        State = 1;
        tx = _tx;
        ty = _ty;
        H = UnitsToTarget();
        F = G + H;
    }
    public void Reset()
    {
        tx = 0;
        ty = 0;
        G = 0;
        H = 0;
        State = 0;
        Parent = null;
    }
    public int UnitsToTarget()
    {
        int dx = tx - x;
        int dy = ty - y;
        return Mathf.RoundToInt(10*Mathf.Sqrt(dx * dx + dy * dy));
    }
    public int UnitsToWorldNode(WorldNode _node)
    {
        /*
         * This function will calculate the G cost from start position to
         * the argumented node, will therefore be this G + units to
         * the coordinates!
        */
        int dx = _node.x - x;
        int dy = _node.y - y;
        return (Mathf.RoundToInt(10 * Mathf.Sqrt(dx * dx + dy * dy)) + G);
    }
    public int UnitsToWorldNode(WorldNode _node,float _mod)
    {
        /*
         * This function will calculate the G cost from start position to
         * the argumented node, will therefore be this G + units to
         * the coordinates!
        */
        int dx = _node.x - x;
        int dy = _node.y - y;
        return (Mathf.RoundToInt(_mod*10 * Mathf.Sqrt(dx * dx + dy * dy)) + G);
    }
    public string ToStr() //Only the coordinates to string.
    {
        return (x.ToString()+"x"+y.ToString());
    }
}

public class NavMap //Like WorldMap, but grid based. Is also a navigator.
{
    public int width, height;
    public int[,] MaterialMap; //Material indexes.
    public int[,] BlockMap;    //Blocks: 1, Obstacles: 2.
    public bool[,] PMap;
    public WorldNode[,] NodeMap;
    const float _C = 2.5f;
    public NavMap(int _width, int _height)
    {
        width = _width;
        height = _height;
        MaterialMap = new int[_width, _height];
        BlockMap = new int[_width, _height];
        NodeMap = new WorldNode[_width, _height];
        PMap = new bool[_width, _height];
        for (int yy = 0; yy < _height; yy++ )
        {
            for (int xx = 0; xx < _width; xx++)
            {
                MaterialMap[xx, yy] = 0;
                BlockMap[xx, yy] = 0;
                PMap[xx, yy] = false;
                NodeMap[xx, yy] = new WorldNode(xx, yy);
            }
        }
    }
    public NavMap(int _width, int _height,int _fill)
    {
        width = _width;
        height = _height;
        MaterialMap = new int[_width, _height];
        BlockMap = new int[_width, _height];
        for (int yy = 0; yy < _height; yy++)
        {
            for (int xx = 0; xx < _width; xx++)
            {
                MaterialMap[xx, yy] = _fill;
                BlockMap[xx, yy] = _fill;
            }
        }
    }
    //TODO This function has to be updated so that xsize pieces can use it:
    public bool Traversable(int x0, int y0, int _dir)
    {
        bool temp = true;
        int x1, y1, x2, y2;
        x2 = Coz(_dir);
        y2 = Zin(_dir);
        x1 = x0+x2;
        y1 = y0+y2;


        if (BlockMap[x1,y1]!=0)
        {
            temp = false;
        }
        else
        {
            if (x2 != 0 && y2 != 0)
            { //Diagonals
                x1 = x0+Coz(_dir - 1);
                y1 = y0+Zin(_dir - 1);

                x2 = x0+Coz(_dir + 1);
                y2 = y0+Zin(_dir + 1);

                if ((BlockMap[x1,y1]==2) || (BlockMap[x2,y2]==2))
                {
                    temp = false;
                }
            }
        }

        return temp;
    }
    public int Coz(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Cos(_dir * Mathf.PI / 4)));
    }
    public int Zin(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Sin(_dir * Mathf.PI / 4)));
    }
    int Dirfy(int _dir)
    {
        int tempDir = _dir;
        while (tempDir < 0) { tempDir += 8; }
        while (tempDir >= 8) { tempDir -= 8; }
        return tempDir;
    }
    int PointDir(Coord p0, Coord p1)
    {
        int TempDir = 0;
        float dx = p1.x - p0.x;
        float dy = p1.y - p0.y;
        float rr = Mathf.Sqrt(dx * dx + dy * dy);
        if (dy > 0)
        {
            TempDir = Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else if (dy < 0)
        {
            TempDir = 8 - Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else if (rr > 0)
        {
            TempDir = Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else
        {
            Debug.LogError("You attempted to check the difference between the same coords");
        }

        return TempDir;

    }
    WorldNode GetLowestNode(List<WorldNode> _nodes)
    {
        WorldNode tempN = _nodes[0];
        int tempV = _nodes[0].F;
        for (int i = 1; i < _nodes.Count; i += 1)
        {
            if (_nodes[i].F < tempV)
            {
                tempN = _nodes[i];
                tempV = _nodes[i].F;
            }
        }
        return tempN;
    }
    public List<Coord> FindPath(int x0, int y0, int x1, int y1)
    {//From pos0 to pos 1
        Coord TempDir = new Coord(0, 0);
        float mod = 3;
        int Loops = 0;
        List<WorldNode> OPEN = new List<WorldNode>();
        List<WorldNode> CLOSED = new List<WorldNode>();
        int xPlus, yPlus, ComparisonG;
        WorldNode Current, Hokano, TempNode, EditNode;
        List<Coord> TempPath = new List<Coord>();

        EditNode = NodeMap[x0, y0];
        OPEN.Add(EditNode); //Adds Editnode to the OPEN lsit.
        EditNode.Set(x1, y1); //Modifies the Editnode.
        Current = EditNode;

        while (OPEN.Count > 0)
        {
            Loops += 1;
            //Find lowest OPEN Node:
            Current = GetLowestNode(OPEN);

            if (Current.H == 0)
            {
                break;
            }
            OPEN.Remove(Current);
            CLOSED.Add(Current);Current.State = 2;

            for (int dir = 0; dir < 8; dir += 1)
            {
                mod = 1;
                xPlus = Current.x + Coz(dir);
                yPlus = Current.y + Zin(dir);
                if ((xPlus < 0 || yPlus < 0) || (xPlus >= width || yPlus >= width))
                    continue;
                if (PMap[xPlus, yPlus]) { mod = _C;}
                Hokano = NodeMap[xPlus, yPlus]; //Hokano = other.
                //Edit.
                ComparisonG = Current.UnitsToWorldNode(Hokano,_C);//Kokokara.

                if (!Traversable(Current.x,Current.y, dir) || Hokano.State==2)
                {
                    continue;
                }
                //if(Node[c0.x,c0.y].color = "green";
                if (Hokano.State==1)
                {
                    if (Hokano.G > ComparisonG)
                    {
                        Hokano.SetParent(Current);
                    }
                }
                else
                {
                    OPEN.Add(Hokano); Hokano.Set(x1,y1,Current,mod);
                }

            }
        }
        //Debug.Log(Loops);
        //Retrace steps:
        TempNode = Current;

        while (TempNode.Parent != null)
        {
            TempPath.Add(new Coord(TempNode.x,TempNode.y));
            TempNode = TempNode.Parent;
        }
        //Clean up and reset Nodes in CLOSED and OPEN list
        foreach (WorldNode Reseter in CLOSED) { Reseter.Reset(); }
        foreach (WorldNode Reseter in OPEN) { Reseter.Reset(); }

        return TempPath;
    }
    public List<Coord> FindPathEx(int x0, int y0,int x1, int y1)
    {
        int Tmptmp = BlockMap[x0, y0];
        BlockMap[x0, y0] = 0; //Temporary sets this to 0 while finding path.
        List<Coord> RetList = FindPath(x1, y1, x0, y0);
        BlockMap[x0, y0] = Tmptmp;
        return RetList;
    }
    public int FindPathDirection(int x0, int y0, int x1, int y1)
    {
        List<Coord> tmp = FindPathEx(x0, y0, x1, y1);
        //Debug.Log(tmp.Count);
        tmp.Add(new Coord(x1, y1));
        return PointDir(tmp[0], tmp[1]);
    }
}

public class WorldMap
{
    public List<string> MapO;
    public List<string> MapX;
    public WorldMap(List<string> lstO, List<string> lstX)
    {
        MapO = lstO;
        MapX = lstX;
    }
    public WorldMap()
    {
        MapO = new List<string>();
        MapX = new List<string>();
    }
    public void PlaceBlock(int x, int z)
    {
        Coord crr = new Coord(x, z);
        MapX.Add(crr.ToStr());
    }
    public void PlaceObstacle(int x, int z)
    {
        Coord crr = new Coord(x, z);
        MapO.Add(crr.ToStr());
    }
}

public class Pathfinder
{
    private List<string> MapO; //Cutable Corners.
    private List<string> MapX; //Not Cutable Corners.
    public Pathfinder(List<string> lstO, List<string> lstX)
    {
        MapO = lstO;
        MapX = lstX;
    }
    public Pathfinder(WorldMap WMap)
    {
        MapO = WMap.MapO;
        MapX = WMap.MapX;
    }
    bool CoordInList(Coord crd0,List<Coord> coordsList)
    {
        bool temp = false;
        for (int i = 0; i < coordsList.Count; i+=1)
        {
            if (coordsList[i].Equals(crd0)){
                temp = true;
                break;
            }
        }
        return temp;
    }
    Node FindNode(Coord crd0, List<Node> NodesList)
    {
        Node tempNode;
        bool temp = false;
        for (int i = 0; i < NodesList.Count; i += 1)
        {
            if ((NodesList[i].position.x ==crd0.x) && (NodesList[i].position.y==crd0.y))
            {
                temp = true;
                tempNode = NodesList[i];
                return tempNode;
            }
        }
        if (!temp){
            Debug.LogError("Could not find the correct coord :(");
        }
        return null;
    }
    bool Traversable(Coord c0, int _dir)
    {
        bool temp = true;
        int xpls, ypls;

        xpls = Coz(_dir);
        ypls = Zin(_dir);
        Coord cPlus;

        cPlus = new Coord(xpls+c0.x, ypls+c0.y);
        if (MapO.Contains(cPlus.ToStr()) || MapX.Contains(cPlus.ToStr()))
        {
            temp = false;
        }
        else
        {
            if (xpls != 0 && ypls != 0)
            { //Diagonals
                xpls = Coz(_dir - 1);
                ypls = Zin(_dir - 1);
                Coord CplusR = new Coord(xpls + c0.x, ypls + c0.y);

                xpls = Coz(_dir + 1);
                ypls = Zin(_dir + 1);
                Coord CplusL = new Coord(xpls + c0.x, ypls + c0.y);

                if (MapX.Contains(CplusL.ToStr()) || MapX.Contains(CplusR.ToStr()))
                {
                    temp = false;
                }
            }
        }

        return temp;
    }


    public int FindDirection(Coord pos0, Coord pos1, int MinUnits) //Finds path, returns step1 dir.
    {//From pos0 to pos 1
        int TempDir = 0;
        int Loops = 0;
        List<Node> OPEN = new List<Node>();
        List<Node> CLOSED = new List<Node>();
        List<string> GreenCrd = new List<string>();
        List<string> RedCrd = new List<string>();
        Coord crdPlus;
        int xPlus, yPlus, ComparisonG;
        Node Current, Hokano, TempNode;
        Coord Pathdir;
        Pathdir = pos0;

        OPEN.Add( new Node(pos0, pos1, 0, -1) );GreenCrd.Add(pos0.ToStr());
        Current = OPEN[0];
        while (OPEN.Count>0)
        {
            Loops += 1;
            //Find lowest OPEN Node:
            Current = GetLowestNode(OPEN);
            //Debug.Log(Current.F);
            if (Current.H<=MinUnits)
            {
                break;
            }
            OPEN.Remove(Current);GreenCrd.Remove(Current.position.ToStr());
            CLOSED.Add(Current);RedCrd.Add(Current.position.ToStr());

            for (int dir = 0; dir < 8; dir+=1)
            {
                xPlus = Current.position.x + Coz(dir);
                yPlus = Current.position.y + Zin(dir);
                crdPlus = new Coord(xPlus, yPlus);
                ComparisonG = Current.G + Current.position.UnitsTo(crdPlus);

                if(RedCrd.Contains(crdPlus.ToStr()) || !Traversable(Current.position,dir) ){
                    continue;
                }
                //Continue from here :)
                if( GreenCrd.Contains(crdPlus.ToStr() ) )
                {
                    Hokano = FindNode(crdPlus, OPEN);
                    if (Hokano.G < ComparisonG)
                    {
                        Hokano.Parent = Current;
                        Hokano.G = ComparisonG;
                        Hokano.UpdateNode();
                    }
                }
                else
                {
                    OPEN.Add(new Node(crdPlus, pos1, ComparisonG, dir,Current)); GreenCrd.Add(crdPlus.ToStr());
                    //Debug.Log(crdPlus.ToStr());
                }
                   
            }
        }
        //Debug.Log(Loops);
        //Retrace steps:
        TempNode = Current;
        while (TempNode.Parent != null)
        {
            Pathdir = TempNode.position;
            TempNode = TempNode.Parent;
        }
        TempDir = PointDir(pos0, Pathdir);

        return TempDir;
    }

    public Coord FindCoord(Coord pos0, Coord pos1, int MinUnits) //Finds path, returns step1 dir.
    {//From pos0 to pos 1
        Coord TempDir = new Coord(0,0);
        int Loops = 0;
        List<Node> OPEN = new List<Node>();
        List<Node> CLOSED = new List<Node>();
        List<string> GreenCrd = new List<string>();
        List<string> RedCrd = new List<string>();
        Coord crdPlus;
        int xPlus, yPlus, ComparisonG;
        Node Current, Hokano, TempNode;
        Coord Pathdir;
        Pathdir = pos0;

        OPEN.Add(new Node(pos0, pos1, 0, -1)); GreenCrd.Add(pos0.ToStr());
        Current = OPEN[0];
        while (OPEN.Count > 0)
        {
            //Find lowest OPEN Node:
            Current = GetLowestNode(OPEN);

            if (Current.H <= MinUnits)
            {
                break;
            }
            OPEN.Remove(Current); GreenCrd.Remove(Current.position.ToStr());
            CLOSED.Add(Current); RedCrd.Add(Current.position.ToStr());

            for (int dir = 0; dir < 8; dir += 1)
            {
                xPlus = Current.position.x + Coz(dir);
                yPlus = Current.position.y + Zin(dir);
                crdPlus = new Coord(xPlus, yPlus);
                ComparisonG = Current.G + Current.position.UnitsTo(crdPlus);

                if (RedCrd.Contains(crdPlus.ToStr()) || !Traversable(Current.position, dir))
                {
                    continue;
                }
                //Continue from here :)
                if (GreenCrd.Contains(crdPlus.ToStr()))
                {
                    Hokano = FindNode(crdPlus, OPEN);
                    //Debug.Log("Is in green");
                    //Debug.Log(Hokano.position.ToStr());
                    if (Hokano.G < ComparisonG)
                    {
                        if (Current == null)
                            Debug.LogError("Current was null");
                        Hokano.Parent = Current;
                        Hokano.G = ComparisonG;
                        Hokano.UpdateNode();
                        Loops += 1;
                        //Debug.Log("Update!");
                    }
                }
                else
                {
                    if (Current == null) { Debug.LogError("You basically fucked up big time!"); }
                    OPEN.Add(new Node(crdPlus, pos1, ComparisonG, dir, Current)); GreenCrd.Add(crdPlus.ToStr());
                }

            }
        }
        //Debug.Log(Loops);
        //Retrace steps:
        TempNode = Current;


        Node testNode;
        while (TempNode.Parent != null)
        {
            Pathdir = TempNode.position;
            TempNode = TempNode.Parent;
        }
         
        return Pathdir;
    }
    public List<Coord> FindPath(Coord pos0, Coord pos1, int MinUnits) //Finds path, returns step1 dir.
    {//From pos0 to pos 1
        Coord TempDir = new Coord(0, 0);
        int Loops = 0;
        List<Node> OPEN = new List<Node>();
        List<Node> CLOSED = new List<Node>();
        List<string> GreenCrd = new List<string>();
        List<string> RedCrd = new List<string>();
        Coord crdPlus;
        int xPlus, yPlus, ComparisonG;
        Node Current, Hokano, TempNode;
        Coord Pathdir;
        List<Coord> TempPath = new List<Coord>();
        Pathdir = pos0;

        OPEN.Add(new Node(pos0, pos1, 0, -1)); GreenCrd.Add(pos0.ToStr());
        Current = OPEN[0];
        while (OPEN.Count > 0)
        {
            Loops += 1;
            //Find lowest OPEN Node:
            Current = GetLowestNode(OPEN);

            if (Current.H <= MinUnits)
            {
                break;
            }
            OPEN.Remove(Current); GreenCrd.Remove(Current.position.ToStr());
            CLOSED.Add(Current); RedCrd.Add(Current.position.ToStr());

            for (int dir = 0; dir < 8; dir += 1)
            {
                xPlus = Current.position.x + Coz(dir);
                yPlus = Current.position.y + Zin(dir);
                crdPlus = new Coord(xPlus, yPlus);
                ComparisonG = Current.G + Current.position.UnitsTo(crdPlus);

                if (!Traversable(Current.position, dir) || RedCrd.Contains(crdPlus.ToStr()) )
                {
                    continue;
                }
                //Continue from here :)
                //if(Node[c0.x,c0.y].color = "green";
                if (GreenCrd.Contains(crdPlus.ToStr()))
                {
                    Hokano = FindNode(crdPlus, OPEN); //Possibly bug.
                    if (Hokano.G > ComparisonG)
                    {
                        if (Current == null)
                            Debug.LogError("Current was null");
                        Hokano.Parent = Current;
                        Hokano.G = ComparisonG;
                        Hokano.UpdateNode();
                        //Debug.Log("Update!");
                    }
                }
                else
                {
                    if (Current == null) { Debug.LogError("You basically fucked up big time!"); }
                    OPEN.Add(new Node(crdPlus, pos1, ComparisonG, dir, Current)); GreenCrd.Add(crdPlus.ToStr());
                }

            }
        }
        //Debug.Log(Loops);
        //Retrace steps:
        TempNode = Current;

        while (TempNode.Parent != null)
        {
            Pathdir = TempNode.position;
            TempPath.Add(Pathdir);
            TempNode = TempNode.Parent;
        }

        return TempPath;
    }

    public int Coz(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Cos(_dir * Mathf.PI / 4)));
    }
    public int Zin(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Sin(_dir * Mathf.PI / 4)));
    }
    int Dirfy(int _dir)
    {
        int tempDir = _dir;
        while (tempDir < 0) { tempDir += 8; }
        while (tempDir >= 8) { tempDir -= 8; }
        return tempDir;
    }
    int PointDir(Coord p0, Coord p1)
    {
        int TempDir = 0;
        float dx = p1.x - p0.x;
        float dy = p1.y - p0.y;
        float rr = Mathf.Sqrt(dx * dx + dy * dy);
        if(dy>0)
        {
            TempDir = Mathf.RoundToInt( Mathf.Acos(dx / rr)*4/Mathf.PI );
        }
        else if(dy<0)
        {
            TempDir = 8-Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else if (rr>0)
        {
            TempDir = Mathf.RoundToInt(Mathf.Acos(dx / rr) * 4 / Mathf.PI);
        }
        else
        {
            Debug.LogError("You attempted to check the difference between the same coords");
        }

        return TempDir;

    }
    Node GetLowestNode(List<Node> _nodes)
    {
        Node tempN = _nodes[0];
        int tempV = _nodes[0].F;
        for (int i = 1; i < _nodes.Count; i+=1)
        {
            if(_nodes[i].F < tempV)
            {
                tempN = _nodes[i];
                tempV = _nodes[i].F;
            }
        }
        return tempN;
    }
}

public struct Path
{
    private List<Coord> coords;
    private float ps;
    public float x, y;
    public int Direction;
    public Path(params Coord[] _coords)
    {
        Direction = 0;
        x = 0f;
        y = 0f;
        ps = 0f;
        coords = new List<Coord>();
        for (int i = 0; i < _coords.Length; i++)
        {
            coords.Add(_coords[i]);
        }
        x = coords[0].x;
        y = coords[0].y;
    }
    public Path(List<Coord> _coords)
    {
        Direction = 0;
        x = 0f;
        y = 0f;
        ps = 0f;
        coords = _coords;
        x = coords[0].x;
        y = coords[0].y;
    }
    public void SetCoords(List<Coord> _coords)
    {
        coords = _coords;
        Update();
    }
    public void SetPos(float pos)
    {
        ps = pos;
        Update();
    }
    public void Update()
    {
        if (ps > 1) { ps = 1; }
        if (ps < 0) { ps = 0; }
        float tmp2 = ps * (coords.Count - 1);
        int tmp = Mathf.FloorToInt(tmp2);
        float tmp3 = tmp2 - tmp;
        if (ps < 1)
        {
            x = coords[tmp].x + (coords[tmp + 1].x - coords[tmp].x) * tmp3;
            y = coords[tmp].y + (coords[tmp + 1].y - coords[tmp].y) * tmp3;
        }
        else
        {
            x = coords[tmp].x;
            y = coords[tmp].y;
        }
        if (tmp<coords.Count-1)
        {
            Direction = Mathz.PointDirection(coords[tmp], coords[tmp + 1]);
        }
        else if (tmp>0)
        {
            Direction = Mathz.PointDirection(coords[tmp-1], coords[tmp]);
        }
        else
        {
            Direction = 0;
        }
    }
};

/*
public class AStar : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
*/
