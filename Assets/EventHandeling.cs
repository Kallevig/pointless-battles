﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventHandeling : MonoBehaviour {
    public KeyCode ControllUp;
    public KeyCode ControllLeft;
    public KeyCode ControllRight;
    public KeyCode ControllDown;
    public KeyCode ControllDiagonal;
    public KeyCode ControllTurn;
    public KeyCode SpawnKey; //Used to test spawning mechanism.
    public List<BattleMechanics> EventObjects;
    public BattleMechanics EventObject;
    public List<int> EventCommands;
    public int EventCommand;
    public int FRAMES; //Assign this later!!!!!!
    BattleMechanics prev;
    int Anim;
    bool ExcSng, Proceed;
    float MaxTime;
    float process;
    bool Action;
    bool addWalkTime; //Whether to add walk time or single event time to the world time.
    int Walkers;
    BattleMechanics tmpBM;
    Transform otr;
    public bool[,] PMap;
    NavMap Navigation;
    Management Mng;
    int WS;
    int HKey;
    int VKey;
    float prevTime;
    float wTime, sTime;
    public int TimeModification;
    int testint;
    // Use this for initialization
    void Awake()
    {
        Mng = GetComponent<Management>();
        WS = Mng.WorldSize;
        PMap = new bool[WS, WS];
        for (int i = 0; i < WS; i++)
        {
            for (int j = 0; j < WS; j++)
            {
                PMap[j, i] = false;
            }
        }
    }

    void Start () {
        HKey = 0;
        VKey = 0;
        EventObjects = new List<BattleMechanics>();
        EventObject = null;
        EventCommands = new List<int>();
        EventCommand = 0;
        Anim = 0;
        ExcSng = false;
        Proceed = false;
        MaxTime = 60f;
        Action = false;
        Walkers = 0;
        Proceed = true;
        Navigation = GameObject.Find("_manager").GetComponent<Management>().Navigator;
        prev = null;
        testint = 0;
        prevTime = Mng.WorldTime;
        wTime = 0;
        sTime = 0;
        addWalkTime = true;
	}
    public void Prolong()
    {
        Anim = 3;
    }
    public bool Traversable(int x0, int y0, int x1, int y1)
    {
        bool tmp = true;
        int tmpDir = Mathz.PointDirection(x0, y0, x1, y1);
        tmp = (Navigation.Traversable(x0, y0, tmpDir) && (!PMap[x1, y1]));
        return tmp;
    }
    public void SetPMap(int x0, int y0, int xsize,bool _set)
    {
        for (int yy = y0 - xsize; yy < y0 + xsize + 1; yy += 1)
        {
            if ((yy < 0) || (yy >= WS)) { continue; }
            for (int xx = x0 - xsize; xx < x0 + xsize + 1; xx += 1)
            {
                if ((xx < 0) || (xx >= WS)) { continue; }
                PMap[xx, yy] = _set;
                Navigation.PMap[xx, yy] = _set;
            }
        }
    }
    public bool Traversable(int x0, int y0, int x1, int y1, BattleMechanics blm)
    {
        bool tmp = true;
        SetPMap(x0, y0, blm.XSize, false);
        for (int yy = y1 - blm.XSize; yy < y1 + blm.XSize + 1; yy+=1)
        {
            if ((yy < 0) || (yy >= WS)) { continue; }
            for (int xx = x1 - blm.XSize; xx < x1 + blm.XSize + 1; xx += 1)
            {
                if ((xx < 0) || (xx >= WS)) { continue; }
                if (PMap[xx,yy])
                {
                    tmp = false;
                }
            }
        }
        SetPMap(x0, y0, blm.XSize, true);
        int tmpDir = Mathz.PointDirection(x0, y0, x1, y1);
        return (Navigation.Traversable(x0, y0, tmpDir) && tmp);
    }
    void TurnUpdate(){
        testint++;
        //Deal with time:
        //Update World time:
        if (addWalkTime){
            Mng.WorldTime += wTime;
            wTime = 0;
        }else{
            Mng.WorldTime += sTime + wTime; //Because passes count as walking.
            sTime = 0;
            wTime = 0;
        }
        float DT = Mng.WorldTime - prevTime;
        //Debug.Log(DT.ToString()+", "+addWalkTime.ToString());
        prevTime = Mng.WorldTime;
        GameObject[] pics = GameObject.FindGameObjectsWithTag("BattlePiece");
        foreach (GameObject gmob in pics)
        {
            BattleMechanics tmp = gmob.GetComponent<BattleMechanics>();
            tmp.OnAction(DT);
        }
    }
    void OnPause(){
        TurnUpdate();
    }
    GameObject GetNext()
    {
        GameObject[] pics = GameObject.FindGameObjectsWithTag("BattlePiece");
        float minDt;
        int chosen = 0;
        int LL = pics.Length;
        BattleMechanics BM;
        BM = pics[0].GetComponent<BattleMechanics>();
        minDt = GetDelta(BM);
        float tmpDt;
        if(LL>1)
        {
            for (int i = 1; i < LL; i++)
            {
                BM = pics[i].GetComponent<BattleMechanics>();
                tmpDt = GetDelta(BM);
                if (tmpDt<minDt)
                {
                    //Debug.Log(tmpDt.ToString() + " < " + minDt.ToString());
                    minDt = tmpDt;
                    chosen = i;
                }
            }
        }
        //Debug.Log(pics[chosen].name);
        //Debug.Log(minDt);
        return pics[chosen];

    }
    float GetDelta(BattleMechanics btm)
    {
        return ((MaxTime-btm.curSpd)/btm.bSpd);
    }
    float GetDelta(GameObject gmOb)
    {
        BattleMechanics btm = gmOb.GetComponent<BattleMechanics>();
        return GetDelta(btm);
    }
    public bool IsFreeSpace(int _x, int _y)
    {
        if (((_x > 0) && (_y > 0)) && ((_x < WS) && (_y < WS)))
        {
            return ((Navigation.BlockMap[_x, _y] == 0) && (PMap[_x, _y] == false));
        }
        else
        {
            return false;
        }
    }
    public bool IsMyTurn()
    {
        return (Proceed == false);
    }
    public void ProceedPlease(int _decision)
    {
        BattleMechanics btl;
        btl = GetNext().GetComponent<BattleMechanics>();
        btl.Decision = _decision;
        btl.ValidateDecision();
        Proceed = true;
    }
    void Susundeyo(BattleMechanics btm,bool singeEvent)
    {
        BattleMechanics BM;
        //Debug.Log(btm.gameObject.name);
        //Debug.Log(btm.curSpd);
        //Debug.Log(btm.gameObject.name);
        float tmpDelta = GetDelta(btm);
        //Mng.WorldTime += tmpDelta * TimeModification;
        if (singeEvent){
            sTime += tmpDelta * TimeModification;
            //Debug.Log("S: " + (tmpDelta * TimeModification).ToString());
        }else{
            wTime += tmpDelta * TimeModification;
            //Debug.Log("W: " + (tmpDelta * TimeModification).ToString());
        }
        btm.curSpd = 0;
        GameObject[] btlP = GameObject.FindGameObjectsWithTag("BattlePiece");
        for (int i = 0; i < btlP.Length; i++)
        {
            BM = btlP[i].GetComponent<BattleMechanics>();
            if (BM==btm)
            {
                continue;
            }
            BM.curSpd += BM.bSpd * tmpDelta;
            //Debug.Log(BM.gameObject.name+": "+BM.curSpd.ToString()+" with delta "+tmpDelta.ToString());
        }

    }

    void ExecuteEvents()
    {
        GameObject[] gmObs = GameObject.FindGameObjectsWithTag("BattlePiece");
        BattleMechanics BM;
        for (int i = 0; i < gmObs.Length; i++)
        {
            BM = gmObs[i].GetComponent<BattleMechanics>();
            if (BM.Walk)
            {
                BM.SelfAnim = FRAMES;
                BM.SetAnimationSpeed(1f*BM.walkPath.Count);
                BM.myPath = new Path(BM.walkPath);
            }
        }
    }
    void LogAll()
    {
        GameObject[] gmObs = GameObject.FindGameObjectsWithTag("BattlePiece");
        BattleMechanics BM;
        string mesg = "";
        for (int i = 0; i < gmObs.Length; i++)
        {
            BM = gmObs[i].GetComponent<BattleMechanics>();
            mesg+=BM.gameObject.name+": "+BM.curSpd.ToString()+"; ";
        }
        //Debug.Log(mesg);
    }

    // Update is called once per frame
    void Update()
    {
        BattleMechanics NextEvent;
        if (Anim == 0)
        {
            if (!Proceed)
            {
                //Keyboard inputs here: Remember to reset decision!
                HKey = 0;
                VKey = 0;
                int tmpdir = -1;
                if (Input.GetKey(ControllDown)){ VKey -= 1; }
                if (Input.GetKey(ControllUp)) { VKey += 1; }
                if (Input.GetKey(ControllLeft)) { HKey -= 1; }
                if (Input.GetKey(ControllRight)) { HKey += 1; }
                float tmpMagn = Mathf.Sqrt(HKey * HKey + VKey * VKey);
                if (tmpMagn>0)
                {
                    tmpdir = Mathz.PointDirection(0, 0, HKey, VKey);
                }

                if (Input.GetKeyDown(SpawnKey)){
                    NextEvent = GetNext().GetComponent<BattleMechanics>();
                    PieceInfo PInfo = new PieceInfo(Mathf.FloorToInt(Random.Range(1, 4)), 50);
                    Mng.Generate(PInfo);
                    PInfo.setTeam(0); //Friendly.
                    Mng.Spawn(PInfo,
                              NextEvent.X ,
                              NextEvent.Y + 4);
                }

                if (Input.GetKey(ControllTurn) && (tmpdir!=-1) && !(Input.GetKey(ControllDiagonal) && (tmpdir % 2 == 0)))
                {
                    NextEvent = GetNext().GetComponent<BattleMechanics>();
                    NextEvent.Direction = tmpdir;
                }
                else
                {
                    if ((Mathf.Sqrt(HKey * HKey + VKey * VKey) > 0) && !(Input.GetKey(ControllDiagonal) && (tmpdir%2==0)) )
                    {
                        NextEvent = GetNext().GetComponent<BattleMechanics>();
                        NextEvent.Decision = 1000 + tmpdir;
                        Proceed = true;
                    }
                }
            }
            if (Proceed)
            {
                if (!ExcSng)
                {
                    if (prev) { prev.EndMyTurn(); }
                    NextEvent = GetNext().GetComponent<BattleMechanics>(); //From the eventstream. Assings event.
                    NextEvent.OnMyTurn();
                    prev = NextEvent;
                    if (!NextEvent.Remote) { NextEvent.GetDecision(); }
                    while ( ((NextEvent.Decision/1000)==1)) //While this is movement;
                    {
                        //EventCommands.Add(NextEvent.Decision);
                        int XPlus, YPlus;
                        XPlus = Mathz.Coz(NextEvent.Decision - 1000) + NextEvent.X;
                        YPlus = Mathz.Zin(NextEvent.Decision - 1000) + NextEvent.Y;
                        //If right conditions start:
                        if ((Traversable(NextEvent.X, NextEvent.Y, XPlus, YPlus,NextEvent) || NextEvent.Swap!=null) && (NextEvent.Decision!=1999))
                        {
                            if (NextEvent.Swap!=null)
                            {
                                int XPlus2 = Mathz.Coz(NextEvent.Swap.OldDecision - 1000) + NextEvent.Swap.X;
                                int YPlus2 = Mathz.Zin(NextEvent.Swap.OldDecision - 1000) + NextEvent.Swap.Y;
                                //PMap[NextEvent.X, NextEvent.Y] = false;
                                //PMap[XPlus, YPlus] = true;
                                NextEvent.Swap.X = XPlus2;
                                NextEvent.Swap.Y = YPlus2;
                                NextEvent.Swap.Walk = true;
                                NextEvent.Swap.walkPath.Add(new Coord(XPlus2, YPlus2));
                                Walkers += 1;
                            }
                            if (NextEvent.Swap == null)
                            {
                                SetPMap(NextEvent.X, NextEvent.Y, NextEvent.XSize, false);
                                SetPMap(XPlus, YPlus, NextEvent.XSize, true);
                            }
                            NextEvent.Swap = null;
                            //PMap[NextEvent.X, NextEvent.Y] = false;
                            //PMap[XPlus, YPlus] = true;
                            NextEvent.X = XPlus;
                            NextEvent.Y = YPlus;
                            NextEvent.Walk = true;
                            NextEvent.walkPath.Add(new Coord(XPlus, YPlus));
                            Walkers += 1;
                        }
                        else
                        {
                            NextEvent.SwapRequest(XPlus, YPlus);
                        }
                        //Susundeyo can be put in the loop above if you want
                        //  movement if and only if it is free way.
                        Susundeyo(NextEvent,false);
                        NextEvent.OldDecision = NextEvent.Decision;
                        NextEvent.Decision = 0;
                        //---end.
                        NextEvent = GetNext().GetComponent<BattleMechanics>();
                        if (!NextEvent.Remote) { NextEvent.GetDecision(); }
                    }

                    if (NextEvent.Decision!=0)
                    {
                        EventObject = NextEvent;
                        EventCommand = NextEvent.Decision;
                        //Debug.Log("Susunda");
                        Susundeyo(NextEvent,true);
                        NextEvent.Decision = 0;
                        ExcSng = true;
                    }
                    else  //Pause when not decided.
                    {
                        if (NextEvent.Swapper != null) {
                            NextEvent.Swapper.Swap = null;
                            NextEvent.Swapper = null;
                        }
                        Proceed = false;
                    }

                    if (Walkers > 0)
                    {
                        Walkers = 0;
                        ExecuteEvents(); //If and only if Evenlist.len >0
                        Anim = FRAMES;
                        addWalkTime = true;
                    }
                }
                else
                {
                    //ExecuteEvents(SingleEvent, 3);
                    Anim = 60;
                    addWalkTime = false;
                    ExcSng = false;
                    //Debug.Log(EventCommand);
                    if (EventCommand/20000 == 1)
                    {
                        int tmpp = EventCommand - 20000;
                        int tmpDir = tmpp / 1000;
                        int tmpInd = tmpp % 1000;
                        EventObject.UseMove(tmpInd, tmpDir);
                    }
                }
            }
        }
        if (Anim!=0)
        {

            if (!Action)
            {
                Action = true;
                //Init stuff here:
            }
            GameObject[] pics = GameObject.FindGameObjectsWithTag("BattlePiece");
            Anim--;
            process = 1f * (FRAMES - Anim) / (1f * FRAMES);
            for (int i = 0; i < pics.Length; i++)
            {
                tmpBM = pics[i].GetComponent<BattleMechanics>();
                if (!tmpBM.Walk)
                {
                    continue;
                }
                tmpBM.Place(process);
            }
            /*
            for each object with a path > 0
            {
                Animate();
            }
            */
        }
        if (Anim==0 && Action)
        {
            Action = false;
            //Reset stuff:
            TurnUpdate();
            GameObject[] pics = GameObject.FindGameObjectsWithTag("BattlePiece");
            process = 0;
            for (int i = 0; i < pics.Length; i++)
            {
                tmpBM = pics[i].GetComponent<BattleMechanics>();
                tmpBM.ResetPos();
                //Reset Path in function "ResetPos()".
            }
        }
    }
}
