﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agro : MonoBehaviour {

    public List<GameObject> AgroObjects;
    List<float> AgroValues;
    public List<float> AgroPoints;
	// Use this for initialization
    public void Add(GameObject gmob, float agro)
    {
        AgroObjects.Add(gmob);
        AgroPoints.Add(agro);
    }
    public void Delete(GameObject btl)
    {
        int tmp=-1;
        for (int i = 0; i < AgroObjects.Count; i++)
        {
            if(AgroObjects[i]==btl)
            {
                tmp = i;
                break;
            }
        }
        if (tmp!=-1)
        {
            AgroObjects.RemoveAt(tmp);
            AgroPoints.RemoveAt(tmp);
        }
    }
    public void Delete(int ind)
    {
        AgroObjects.RemoveAt(ind);
        AgroPoints.RemoveAt(ind);
    }
    public void Clear()
    {
        AgroObjects.Clear();
        AgroPoints.Clear();
    }
    public void UpdateAgro()
    {
        for (int i = 0; i < AgroObjects.Count; i++)
        {
            if (AgroPoints[i]<=0)
            {
                Delete(i);
            }
        }
    }
    public GameObject GetAgroObject() //Rolls values between 0 and agro point value.
    {
        if (AgroObjects.Count > 0)
        {
            float tmpMax = -1;
            float tmp;
            int chosen = 0;
            for (int i = 0; i < AgroObjects.Count; i++)
            {
                tmp = Random.Range(0, AgroPoints[i]);
                if (tmp > tmpMax) { tmpMax = tmp; chosen = i; }

            }
            return AgroObjects[chosen];
        }
        else
        {
            return null;
        }
    }
    public void Decay(float k)
    {
        for (int i = 0; i < AgroObjects.Count; i++)
        {
            AgroPoints[i] -= 1 + AgroPoints[i]*k;
        }
    }
    public void AddAgro(GameObject btl, float amount)
    {
        if (btl != null)
        {
            if (!AgroObjects.Contains(btl))
            {
                Add(btl, 1);
            }
            int tmp = AgroObjects.IndexOf(btl);
            AgroPoints[tmp] += amount;
        }
    }
    public float AgroSum()
    {
        float tmp = 0;
        foreach (float i2 in AgroPoints)
        {
            tmp += i2;
        }
        return (tmp);
    }
    public float AgroValue(GameObject btl)
    {
        if(AgroObjects.Contains(btl))
        {
            return (AgroPoints[AgroObjects.IndexOf(btl)] / AgroSum());
        }
        else
        {
            return 0f;
        }
    }
    void Start()
    {
        AgroObjects = new List<GameObject>();
        AgroPoints = new List<float>();
    }
}
