﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Locomotion : MonoBehaviour {
    Transform trn;
    public int X, Y;
    public float x, y, z;
    public float sstep,sstep2;
    int animFrame;
    public float sspd;
    public int Direction;
    int LockedDirection;
    public bool Moving;
    bool Moved = false;
    public int ocX, ocY;
    Management Mng;
    Animator CharAnim;
	// Use this for initialization
	void Start () {
        trn = transform.GetChild(0).transform.GetChild(0).GetComponent<Transform>();
        z = transform.position.y;
        x = transform.position.x;
        y = transform.position.z;
        GridPosition();
        sspd = 0;
        sstep = 0f;
        animFrame = 0;
        SetTransform();
        LockedDirection = 0;
        Moving = false;
        CharAnim = transform.GetChild(0).transform.GetChild(0).transform.GetChild(0).GetComponent<Animator>();
        Mng = GameObject.Find("_manager").GetComponent<Management>();
        Mng.PlaceBlock(transform.position);
    }
    public int Coz(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Cos(_dir * Mathf.PI / 4)));
    }
    public int Zin(int _dir)
    {
        return (Mathf.RoundToInt(Mathf.Sin(_dir * Mathf.PI / 4)));
    }
    public void GridPosition(){
        X = Mathf.RoundToInt(x);
        Y = Mathf.RoundToInt(y);
        x = X;
        y = Y;
        SetTransform();

    }
    public void SetTransform(){
        transform.position = new Vector3(x, z, y);
        trn.transform.rotation = Quaternion.Euler(0, (10-Direction) * 45, 0);
    }
	
	// Update is called once per frame
	void Update () {
        if (animFrame > 0) { animFrame -= 1; }
        if(animFrame>0){
            CharAnim.SetInteger("AnimationIndex", 1);
        }else{ CharAnim.SetInteger("AnimationIndex", 0); }
	}
    void MovingUpdate(){
        if (Moving)
        {
            animFrame = 2;
            if (sstep >= sspd)
            {
                sstep -= sspd;
                x += Mathf.Cos(LockedDirection * Mathf.PI / 4) * sspd;
                y += Mathf.Sin(LockedDirection * Mathf.PI / 4) * sspd;
            }
            else if (sstep < sspd && sstep>0)
            {
                x += Mathf.Cos(LockedDirection * Mathf.PI / 4) * sstep;
                y += Mathf.Sin(LockedDirection * Mathf.PI / 4) * sstep;
                sstep = 0;
            }
            if (sstep<0.1f*sspd) //Epsilon = 0.1s. 0 ish...
            {
                sstep = 0;
                Moving = false;
                GridPosition();
            }
        }
    }
    public void UpdateWalkspeed(float newSpd){
        if(Moving){
            //sstep = distance left.
            int newFrames2 = Mathf.RoundToInt(sstep / newSpd); //The amount of frames needed.
            int NormalFrames = Mathf.RoundToInt(1f / newSpd);
            sspd = sstep / newFrames2;
            int FPS = 60;
            int WW = 16;
            int ffps = 24;
            float AnimSpeed = FPS * WW / (ffps * NormalFrames);
            CharAnim.SetFloat("AnimationSpeed", AnimSpeed);
            //fps = 24*v
            //W = 16
            //fps * t = W
            //FPS=1./deltatime aka 60
            //t = newFrames2/FPS
            //t = 1/60
            //newFrames*fps/60 = W
            //24*v*newFrames/60 = W
            //v = 60W/(24*newFrames)
        }
    }

    private void LateUpdate() //KeyInputs and changes comes before :)
    {
        MovingUpdate();
        SetTransform();
    }
    public void Turn(int newDirection){
        Direction = newDirection;
    }
    public void Walk(float newSpd){
        if(!Moving && Mng.Navigator.Traversable(X,Y,Direction)){
            CharAnim.SetInteger("AnimationIndex", 1);
            Moving = true;
            int ddx, ddy;
            ddx = Coz(Direction);
            ddy = Zin(Direction);
            sstep = Mathf.Sqrt(ddx * ddx + ddy * ddy);
            UpdateWalkspeed(newSpd);
            LockedDirection = Direction;
            Mng.PlaceBlock(X + ddx, Y + ddy);
            Mng.FreeSpace(X, Y);
        }
    }
    public void TakeStep(int direc,float wlkspd){
        if(!Moving){
            Turn(direc);
            Walk(wlkspd);
        }
    }
}
