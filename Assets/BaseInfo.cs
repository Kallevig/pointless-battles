﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseInfo
{
    public int index;
    public string name;
    public string description;
    public int hp, atk, def, str, agi, dge, hit, crt, lck, hprec, enrec;
    public int bxp, bwt, mem; //bxp = base experience, bwt = base weight, mem = memory.
    public int modelIndex;
    public int xsize;
    List<int> mvInd; //Move indexes.
    List<int> mvLv;  //Move level, at which level a certain move is learned.
    public BaseInfo(string _name,int _model,int _hp, int _atk, int _def, int _str,
                    int _agi, int _dge, int _hit, int _crt, int _lck,
                    int _hprec, int _enrec, int _bxp, int _bwt, int _mem){
        name = _name;
        description = "";
        hp = _hp;
        atk = _atk;
        def = _def;
        str = _str;
        agi = _agi;
        dge = _dge;
        hit = _hit;
        crt = _crt;
        lck = _lck;
        hprec = _hprec;
        enrec = _enrec;
        bxp = _bxp;
        bwt = _bwt;
        mem = _mem;
        mvInd = new List<int>();
        mvLv = new List<int>();
        modelIndex = _model;
        xsize = 0;
    }

    public void AddMove(int moveIndex, int atLevel=1){
        mvInd.Add(moveIndex);
        mvLv.Add(atLevel);
    }

    public void AddMove(Attack move, int atLevel=1){
        mvInd.Add(move.selfIndex);
        mvLv.Add(atLevel);
    }

    public List<int> getMovesAtLv(int lvMin=1, int lvMax=100){
        List<int> tmp = new List<int>();
        for (int i = 0; i < mvInd.Count; i++){
            if (  (mvLv[i] >= lvMin) && (mvLv[i]<=lvMax) ){
                tmp.Add(mvInd[i]);
            }
        }
        if (tmp.Count > 0)
        {
            return tmp;
        }else{
            return null;
        }
    }
}