﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBarBehaviour : MonoBehaviour {

    CharBar CB;
    Management MG;
	// Use this for initialization
	void Start () {
        MG = GameObject.Find("_manager").GetComponent<Management>();
        CB = GetComponent<CharBar>();
	}
	
	// Update is called once per frame
	void Update () {
        CB.Master = MG.View;
	}
}
