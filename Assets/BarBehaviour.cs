﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/*
 * NullReferenceException: Object reference not set to an instance of an object
BarBehaviour.UpdateInfo () (at Assets/BarBehaviour.cs:60)
BarBehaviour.LateUpdate () (at Assets/BarBehaviour.cs:79)
*/

public class BarBehaviour : MonoBehaviour {
    public RenderTexture Settings;
    public GameObject Master;
    GameObject oM; //Old Master;
    RenderTexture RT;
    GameObject MasterCam;
    RawImage Img;
    RectTransform hp, eg, xp;
    TextMeshProUGUI chrt,hpt, egt;
    BattleMechanics btl;
	// Use this for initialization
	void Start () {
        RT = new RenderTexture(Settings);
        Img = transform.GetChild(0).GetChild(0).gameObject.GetComponent<RawImage>();
        Img.texture = RT;
        hp = transform.GetChild(1).GetChild(1).gameObject.GetComponent<RectTransform>();
        eg = transform.GetChild(2).GetChild(1).gameObject.GetComponent<RectTransform>();
        xp = transform.GetChild(3).GetChild(1).gameObject.GetComponent<RectTransform>();

        chrt = transform.GetChild(4).gameObject.GetComponent<TextMeshProUGUI>();
        hpt = transform.GetChild(5).gameObject.GetComponent<TextMeshProUGUI>();
        egt = transform.GetChild(6).gameObject.GetComponent<TextMeshProUGUI>();
        if (Master!=null)
        {
            SetMaster(Master);
            oM = Master;
        }
	}
	void SetMaster(GameObject _Master)
    {
        if (MasterCam != null)
        {
            MasterCam.SetActive(false); //Disable old camera of old master.
        }
        if (_Master != null)
        {
            Master = _Master; //Set new master;
            btl = Master.GetComponent<BattleMechanics>();
            MasterCam = Master.transform.GetChild(0).GetChild(0).GetChild(1).GetChild(0).gameObject;
            MasterCam.SetActive(true);
            Camera _cam = MasterCam.GetComponent<Camera>();
            _cam.targetTexture = RT;
        }
        else
        {
            Master = null;
            MasterCam = null;
        }
    }
    void UpdateInfo()
    {
        if (Master != null)
        {
            hp.localScale = new Vector3(1f * btl.hp / btl.HP, 1, 1);
            eg.localScale = new Vector3(1f * btl.cStr / btl.Str, 1, 1);
            xp.localScale = new Vector3(1f * btl.curXp / btl.NextXp, 1, 1);
            chrt.text = btl.Name + " Lv: " + btl.Lv.ToString();
            hpt.text = btl.hp.ToString() + " / " + btl.HP.ToString();
            egt.text = btl.cStr.ToString() + " / " + btl.Str.ToString();
        }
    }
	// Update is called once per frame
	void LateUpdate () {
        if (Master!=oM)
        {
            SetMaster(Master);
            oM = Master;
        }
        UpdateInfo();
    }
}
