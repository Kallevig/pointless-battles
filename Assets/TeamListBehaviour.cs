﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamListBehaviour : MonoBehaviour {
    public GameObject Master;
    int Size;
    Stats MasterStats;
    List<GameObject> CharBars;
    public List<GameObject> Team;
    public GameObject cBar;
    public float Dist;
    float DD;
	// Use this for initialization
	void Start () {
        CharBars = new List<GameObject>();
        DD = 0;
		
	}
    List<GameObject> GetList()
    {
        MasterStats = Master.GetComponent<Stats>();
        int tmp = MasterStats.team;
        Stats tmpStat;
        List<GameObject> tmpl = new List<GameObject>();
        GameObject[] tmpl2 = GameObject.FindGameObjectsWithTag("BattlePiece");
        foreach (GameObject gmb in tmpl2)
        {
            if (gmb == Master) { continue; }
            tmpStat = gmb.GetComponent<Stats>();
            if (tmpStat.team==tmp)
            {
                tmpl.Add(gmb);
            }
        }
        return tmpl;
    }
	// Update is called once per frame
	void Update () {
        Team = GetList();
        while (Team.Count > CharBars.Count)
        {
            Vector3 tmpPos = (transform.position + new Vector3(0, -1, 0) * (DD));
            GameObject tmp = Instantiate(cBar,tmpPos,transform.rotation,transform);
            tmp.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            CharBars.Add(tmp);
            DD += Dist;
        }
        GameObject ers;
        while (Team.Count < CharBars.Count)
        {
            ers = CharBars[CharBars.Count - 1];
            CharBars.Remove(ers);
            Destroy(ers);
        }
        //Assign Masters.

        for (int i = 0; i < Team.Count;i++)
        {
            CharBar tmpC = CharBars[i].GetComponent<CharBar>();
            tmpC.Master = Team[i];
        }

	}
}
