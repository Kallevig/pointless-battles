﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrowthAnimation : MonoBehaviour {

    public AnimationCurve Curve;
    public float step;
    public float minSize, maxSize;
    float maxtime;
    SelfDestructTimer tm;
    float scaleMod;
    bool Imploded;
    public float delTimer;
    public GameObject DOb;
	// Use this for initialization
	void Start () {
        tm = GetComponent<SelfDestructTimer>();
        maxtime = tm.time;
        step = 0;
        scaleMod = transform.localScale.y;
        Imploded = false;
        UpdateScale();
		
	}
	
	// Update is called once per frame
	void Update () {
        step = (maxtime - tm.time) / maxtime;
        if (step>delTimer && !Imploded){
            Destroy( DOb);
            Imploded = true;
        }

        UpdateScale();
	}
    void UpdateScale()
    {
        float tmp = (Curve.Evaluate(step) * (maxSize - minSize) + minSize) * scaleMod;
        transform.localScale = new Vector3(tmp, tmp, tmp);
    }
}
