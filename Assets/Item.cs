﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Item
{
    public string Name,Description;
    public int value,stock,MaxStock;
    public Item(string _name)
    {
        Name = _name;
        Description = "";
        value = 0;
        stock = 1;
        MaxStock = 1;
    }
    public Item(string _name, string _description)
    {
        Name = _name;
        Description = _description;
        value = 0;
        stock = 1;
        MaxStock = 1;
    }
}

/*
 * Types of items:
 * Equipables own struct
 * Move / Effect - in battle items, assigned an move id
 * Healing items
 * Potions: Stat Buffs / Temporary buffs
 * SoulStones
 * Pets / Companions: use to summon pet in or outside battle.
 *      Damage variable to each item if thrown, and whether it will be
 *      destroyed upon impact and success rate of impact. 3 vars.
 */